//
//  NotificationVC.swift
//  StadiumUser
//
//  Created by apple on 06/06/21.
//  Copyright © 2021 XcelTec-053. All rights reserved.
//

import UIKit

class NotificationCell: UITableViewCell {
    
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var btnImage: UIButton!
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblInfo: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        btnImage.layer.cornerRadius = btnImage.frame.height/2
        btnImage.layer.masksToBounds = true
    }
}

class NotificationVC: UIViewController {

    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var btnRead: UIButton!
    
    @IBOutlet weak var lblNodata: UILabel!
    
    private var arrNotification: [NotificationData] = []
    
    @IBOutlet weak var imgBG: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        tblView.delegate = self
        tblView.dataSource = self
        
        tblView.tableFooterView = UIView()
        tblView.separatorStyle = .none
        
        lblNodata.isHidden = true
        imgBG.isHidden = false
        
        getNotification()
    }
    
    @IBAction func btnReadTapped(_ sender: UIButton) {
        if self.arrNotification.count != 0{
            self.updateNotification()
        }
    }
    
    @IBAction func btnBackTapped(_ sender: UIButton) {
        Helper.sharedHelper.goToHome()
//        self.navigationController?.popViewController(animated: true)
    }
}

extension NotificationVC: UITableViewDataSource, UITableViewDelegate{
        
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrNotification.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationCell", for: indexPath) as! NotificationCell
        
        let noti = arrNotification[indexPath.row]
        
        cell.bgView.backgroundColor = UIColor.white
        
        cell.lblTime.text = ""
        print("\(indexPath.row).\(String(describing: noti.created_date))")
        if let dateStr = noti.created_date{
            let dateFormatter = DateFormatter()
            dateFormatter.locale = Locale(identifier: "en_US_POSIX") // set locale to reliable US_POSIX

            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.sssZ"
            
            if let date = dateFormatter.date(from:dateStr){
//                dateFormatter.dateFormat = "dd MM YYYY HH:mm:ss"
//
//                let dateNew = dateFormatter.string(from: date)
                print("today:\(Date())")
                
                cell.lblTime.text = JSON(date.timeAgoSinceNow as Any).stringValue + " ago"
            
                /*if let mmDate = dateFormatter.date(from: dateNew){
                    cell.lblTime.text = JSON(mmDate.timeAgoSinceNow as Any).stringValue
                }else{
                    cell.lblTime.text = ""
                }*/
            }else{
                debugPrint("Error")
                cell.lblTime.text = ""
            }
        }else{
            debugPrint("Error")
            cell.lblTime.text = ""
        }
        
        cell.lblTitle.text = noti.title
        cell.lblInfo.text = noti.descriptionField
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = Main_STORY.instantiateViewController(withIdentifier: "MyOrderListVC") as! MyOrderListVC
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
}

extension NotificationVC{
    
    private func getNotification(){
        var parameter = [String: Any]()
        
        parameter["device_type"] = 1
        parameter["limit"]  = 20
        parameter["page"]  = 1
        
        print(parameter)
        
        NetworkManager.shared.apiCallWithToken(Api: GETNOTIFICATION, param: parameter , getView: self) { (response) in
            if let getdata = response as? NSDictionary {
                print(getdata)
                
                if let data = getdata.value(forKey: DATA) as? String{
                    let DES = CryptoJS.AES()
                    let decryptedString = DES.decrypt(data, password: "rswna0hu8t")
                    print(decryptedString)
                    
                    let DecryptedData = decryptedString.data(using: .utf8)!
                    do{
                        let output = try JSONSerialization.jsonObject(with: DecryptedData, options: .allowFragments)
                        let array = JSON(output as Any).arrayValue
                        self.arrNotification.removeAll()
                        
                        for item in array {
                            let floor = NotificationData(fromJson: item)
                            self.arrNotification.append(floor)
                        }
                        self.imgBG.isHidden = self.arrNotification.count != 0
                        self.tblView.reloadData()
                    }catch{
                        debugPrint(error.localizedDescription)
                    }
                }else{
                }
            } else {
                print("Response is not in json")
            }
        }
    }
    
    private func updateNotification(){
        var parameter = [String: Any]()
        
        parameter["device_type"] = 1
        parameter["user_id"]  = JSON(USERDEFAULT.value(forKey: Kuser_id) as Any).intValue
        
        print(parameter)
        
        NetworkManager.shared.apiCallWithToken(Api: READNOTIFICATION, param: parameter , getView: self) { (response) in
            if let getdata = response as? NSDictionary {
                print(getdata)
                
                if let data = getdata.value(forKey: DATA) as? String{
                    let DES = CryptoJS.AES()
                    let decryptedString = DES.decrypt(data, password: "rswna0hu8t")
                    print(decryptedString)
                    
                    self.getNotification()
                }else{
                    self.getNotification()
                }
            } else {
                print("Response is not in json")
            }
        }
    }
}

extension Date {
    
    func getElapsedInterval() -> String {
        
        let interval = Calendar.current.dateComponents([.year, .month, .day], from: self, to: Date())
        
        if let year = interval.year, year > 0 {
            return year == 1 ? "\(year)" + " " + "year ago" :
                "\(year)" + " " + "years ago"
        } else if let month = interval.month, month > 0 {
            return month == 1 ? "\(month)" + " " + "month ago" :
                "\(month)" + " " + "months ago"
        } else if let day = interval.day, day > 0 {
            return day == 1 ? "\(day)" + " " + "day ago" :
                "\(day)" + " " + "days ago"
        } else {
            return "a moment"
        }
    }
}

extension Date {
    
    var timeAgoSinceNow: String {
        return getTimeAgoSinceNow()
    }
    
    private func getTimeAgoSinceNow() -> String {
        
        var interval = Calendar.current.dateComponents([.year], from: self, to: Date()).year!
        if interval > 0 {
            return interval == 1 ? "\(interval)" + " year" : "\(interval)" + " years"
        }
        
        interval = Calendar.current.dateComponents([.month], from: self, to: Date()).month!
        if interval > 0 {
            return interval == 1 ? "\(interval)" + " month" : "\(interval)" + " months"
        }
        
        interval = Calendar.current.dateComponents([.day], from: self, to: Date()).day!
        if interval > 0 {
            return interval == 1 ? "\(interval)" + " day" : "\(interval)" + " days"
        }

        interval = Calendar.current.dateComponents([.hour], from: self, to: Date()).hour!
        if interval > 0 {
            return interval == 1 ? "\(interval)" + " hour" : "\(interval)" + " hours"
        }

        interval = Calendar.current.dateComponents([.minute], from: self, to: Date()).minute!
        if interval > 0 {
            return interval == 1 ? "\(interval)" + " minute" : "\(interval)" + " minutes"
        }

        return "a moment"
    }
}
