//
//  OrderCompleteVC.swift
//  StadiumRunner
//
//  Created by XcelTec-053 on 05/04/21.
//  Copyright © 2021 XcelTec-053. All rights reserved.
//

import UIKit

class OrderCompleteVC: UIViewController {

    @IBOutlet weak var lblOrderno: UILabel!
    var orderiD:Int?
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        lblOrderno.text = "You have completed \(String(describing: orderiD!)) "
    }
    
    @IBAction func btnBackToHome(_ sender: UIButton) {
        Helper.sharedHelper.goToHome()
    }
}
