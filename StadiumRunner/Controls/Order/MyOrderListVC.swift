//
//  MyOrderList.swift
//  StadiumRunner
//
//  Created by XcelTec on 16/08/21.
//  Copyright © 2021 XcelTec-053. All rights reserved.
//

import Foundation
import UIKit

class MyOrderListVC: SuperViewController {
    
    @IBOutlet weak var tblViewListing: UITableView!
    @IBOutlet weak var lblHeaderTitle: UILabel!
    @IBOutlet weak var lblCompleted: UILabel!
    @IBOutlet weak var lblDeclined: UILabel!
    @IBOutlet weak var viewDeclinedOrder: UIView!
    @IBOutlet weak var viewCompletedOrder: UIView!
    
    var isNewOrder : Bool = true

    var arrCompletedOrderList = NewOrderData()
    var arrDeclinedOrderList = NewOrderData()

    override func viewDidLoad() {
        
        getCompletedOrderData()
    }

    @IBAction func btnCompletedActions(_ sender: UIButton) {
        viewCompletedOrder.backgroundColor = #colorLiteral(red: 0.9215686275, green: 0.1215686275, blue: 0.231372549, alpha: 1)
        viewDeclinedOrder.backgroundColor = .clear
        isNewOrder = true
        lblDeclined.textColor = #colorLiteral(red: 0.9215686275, green: 0.1215686275, blue: 0.231372549, alpha: 1)
        lblCompleted.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        lblCompleted.font = UIFont(name: "Segoe UI Bold", size: 14.0)
        lblDeclined.font = UIFont(name: "Segoe UI", size: 14.0)
        lblHeaderTitle.text = "Completed Order"
        tblViewListing.reloadData()
        getCompletedOrderData()
    }
    
    @IBAction func btnDeclinedACtions(_ sender: UIButton) {
        viewCompletedOrder.backgroundColor = .clear
        viewDeclinedOrder.backgroundColor = #colorLiteral(red: 0.9215686275, green: 0.1215686275, blue: 0.231372549, alpha: 1)
        isNewOrder = false
        lblCompleted.textColor = #colorLiteral(red: 0.9215686275, green: 0.1215686275, blue: 0.231372549, alpha: 1)
        lblDeclined.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        lblDeclined.font = UIFont(name: "Segoe UI Bold", size: 14.0)
        lblCompleted.font = UIFont(name: "Segoe UI", size: 14.0)
        lblHeaderTitle.text = "Declined Order"
        tblViewListing.reloadData()
        getDeclinedOrderList()
    }
    
    @IBAction func onClickBAck(_ sender: UIButton) {
        Helper.sharedHelper.goToHome()
    }
}

//MARK:- API Call
extension MyOrderListVC{
    
    private func getCompletedOrderData(){
        var parameter = [String: Any]()
        let runnerId = USERDEFAULT.value(forKey: Kuser_id)
        parameter = ["runner_id":runnerId ?? "0"] as [String : Any]
        debugPrint(parameter)
        
        NetworkManager.shared.apiCallWithToken(Api: COMPLETEORDERLIST_URL, param: parameter , getView: self) { (response) in
            if let getdata = response as? NSDictionary {
                print(getdata)
                
                if let data = getdata.value(forKey: DATA) as? String{
                    let DES = CryptoJS.AES()
                    let decryptedString = DES.decrypt(data, password: "rswna0hu8t")
                    print(decryptedString)
                    
                    let DecryptedData = decryptedString.data(using: .utf8)!
                    do{
                        let ModelData = try JSONDecoder().decode(NewOrderData.self, from: DecryptedData)
                        self.arrCompletedOrderList = ModelData

                        print(self.arrCompletedOrderList as Any)
                        
                        self.tblViewListing.reloadData()
                    }catch{
                        debugPrint(error.localizedDescription)
                    }
                } else {
                    print("No Data")
                }
            } else {
                print("Response is not in json")
            }
        }
    }
    
    private func getDeclinedOrderList(){
        var parameter = [String: Any]()
        let runnerId = USERDEFAULT.value(forKey: Kuser_id)
        parameter = ["runner_id":runnerId ?? "0"] as [String : Any]
        debugPrint(parameter)
        
        NetworkManager.shared.apiCallWithToken(Api: REJECTORDERLIST_URL, param: parameter , getView: self) { (response) in
            if let getdata = response as? NSDictionary {
                print(getdata)
                
                if let data = getdata.value(forKey: DATA) as? String{
                    let DES = CryptoJS.AES()
                    let decryptedString = DES.decrypt(data, password: "rswna0hu8t")
                    print(decryptedString)
                    
                    let DecryptedData = decryptedString.data(using: .utf8)!
                    
                    do{
                        let ModelData = try JSONDecoder().decode(NewOrderData.self, from: DecryptedData)
                        self.arrDeclinedOrderList = ModelData

                        print(self.arrDeclinedOrderList as Any)
                        
                        self.tblViewListing.reloadData()
                    }catch{
                        debugPrint(error.localizedDescription)
                    }
                } else {
                    print("No Data")
                }
            } else {
                print("Response is not in json")
            }
        }
    }
    
    @objc func OrderDetails(sender: UIButton){
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "OrderDetailsVC") as! OrderDetailsVC
        vc.orderId = sender.tag
        vc.comefrom = .myorder  
        APP_DELEGATE.window?.rootViewController = vc

    }
}

extension MyOrderListVC : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if isNewOrder {
            if let count = arrCompletedOrderList.getData?.count {
                tableView.setEmptyView(title: "", message: "", messageImage: UIImage())
                return count
            } else {
                tableView.setEmptyView(title: "", message: "Completed order not available", messageImage: UIImage())
                return 0
            }
        } else {
            if let count = arrDeclinedOrderList.getData?.count {
                tableView.setEmptyView(title: "", message: "", messageImage: UIImage())
                return count
            } else {
                tableView.setEmptyView(title: "", message: "Declined order not available", messageImage: UIImage())
                return 0
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "OrderCellClass") as! MyOrderCellClass

        var data : GetData?
        
        if isNewOrder == true {
            data = arrCompletedOrderList.getData?[indexPath.row]
            cell.lblCancelled.isHidden = true
            cell.ConstantViewHeight.constant = 30
        }else{
            cell.lblCancelled.isHidden = false
            data = arrDeclinedOrderList.getData?[indexPath.row]
            cell.ConstantViewHeight.constant = 0
        }

        var strProducts: String?
        for element in data?.products?.flatMap({$0.product_name}) ?? [""] {
            if strProducts == nil {
                strProducts = element
            } else {
                strProducts = strProducts! + ", " + element
            }
        }

        cell.lblFoodName.text = strProducts ?? ""
        cell.lblOrderNumber.text = "Order No : \(data?.order_id ?? 1)"
        cell.lblOrderType.text = "Order Type : \(data?.order_type ?? "")"
        cell.lblTotalAmt.text = "Total Amount : $\(data?.total_amount ?? 0.0)"
       // cell.lblTime.text = String.getFormattedDate(string: data?.created_date ?? "", formatter: "HH:mm a")

        cell.btnOrderDetails.tag = data?.order_id ?? 1
        cell.btnOrderDetails.addTarget(self, action: #selector(OrderDetails(sender:)), for: .touchUpInside)
        
        
        if let dateStr = data?.created_date!{
            let dateFormatter = DateFormatter()
            dateFormatter.locale = Locale(identifier: "en_US_POSIX") // set locale to reliable US_POSIX
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.sssZ"
            
            
            if let date = dateFormatter.date(from:dateStr){
                dateFormatter.dateFormat = "dd-MMM-yyyy"//"dd-MMM-yyyy, h:mm a"
//
                
                let dateAsString = dateFormatter.string(from: date)
               

                //print("\(indexPath.row).\(data!.created_date!)")
                cell.lblTime.text = dateAsString//JSON(date.timeAgoSinceNow as Any).stringValue

            }else{
                debugPrint("Error")
  //              cell.lblTime.text = ""
                cell.lblTime.text = ""
            }
        }else{
            debugPrint("Error")
            cell.lblTime.text = ""
        }
        
        
        return cell
    }
    
    @objc func btnAcceptedActions(sender: UIButton) {
        print(sender.tag)
    }
    
}

class MyOrderCellClass: UITableViewCell {
    @IBOutlet weak var ConstantViewHeight: NSLayoutConstraint!
    @IBOutlet weak var lblFoodName: UILabel!
    @IBOutlet weak var lblCancelled: UILabel!

    @IBOutlet weak var lblOrderNumber: UILabel!
    @IBOutlet weak var lblOrderType: UILabel!
    @IBOutlet weak var lblTotalAmt: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var btnOrderDetails: UIButton!
}
