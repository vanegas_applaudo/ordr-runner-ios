//
//  OrderDetailsVC.swift
//  StadiumRunner
//
//  Created by XcelTec-053 on 06/04/21.
//  Copyright © 2021 XcelTec-053. All rights reserved.
//

import UIKit
//import FlexibleSteppedProgressBar
enum VCS{
    case myorder
    case dashboard
}
class OrderDetailsVC: SuperViewController{

    @IBOutlet weak var ConstantViewMainHeight: NSLayoutConstraint!
    @IBOutlet weak var ConstantViewHeight: NSLayoutConstraint!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var ConstantTblViewHeight: NSLayoutConstraint!
    @IBOutlet weak var tblViewItem: UITableView!
    
    
    @IBOutlet weak var btnDoneHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblResName: UILabel!
    @IBOutlet weak var imgRes: UIImageView!
    
    
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var callResView: UIView!
    @IBOutlet weak var flexStepper: FlexibleSteppedProgressBar!
    
    
    @IBOutlet weak var lblOrderTIme: UILabel!
    @IBOutlet weak var lblCustomerName: UILabel!
    @IBOutlet weak var lblCustomerContactNo: UILabel!
    
    @IBOutlet weak var lblResNameDetail: UILabel!
    @IBOutlet weak var lblCustAddress: UILabel!
    
    @IBOutlet weak var lblOrderNo: UILabel!
    @IBOutlet weak var lblItemsCount: UILabel!
    
    
    @IBOutlet weak var lblTotalAmount: UILabel!
    
    @IBOutlet weak var btnDone: UIButton!
    
    var orderId:Int = 0
    var orderData:GetDatum?
    var orderstatus:orderStatus = .Prepare
    var comefrom:VCS = .dashboard
    var timer = Timer()
    var isTblReloaded = false
    @IBOutlet weak var lblCallRestaurent: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        imgRes.layer.cornerRadius = imgRes.bounds.size.width/2.0
        imgRes.layer.masksToBounds = true
        getOrderDetails()
        setupTimer()
    }
    func setupTimer(){
        if orderstatus != .Dispatched {
            timer =  Timer.scheduledTimer(timeInterval: 10.0, target: self, selector: #selector(callDetails), userInfo: nil, repeats: true)
        }
    }
    
    @objc func callDetails(){
        print("Timer called.\(Date())")
        if orderstatus != .Dispatched {
            getOrderDetailsNoLoading()
        }else{
            timer.invalidate()
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        timer.invalidate()
    }
    override func viewDidLayoutSubviews() {
        ConstantTblViewHeight.constant = self.tblViewItem.contentSize.height
    }
    
    //MARK:- BTN ACTIONS
    
    @IBAction func onClickReload(_ sender: UIButton) {
        getOrderDetails()
    }
    
    @IBAction func onClickBack(_ sender: UIButton) {
        if comefrom == .dashboard {
            Helper.sharedHelper.goToHome()
        }else if comefrom == .myorder {
            Helper.sharedHelper.gotoMyOrder()
        }
    }
    
    @IBAction func onClickCallRes(_ sender: UIButton) {
        var phoneNO = ""
        if sender.titleLabel?.text?.contains("Restaurant") != nil
        {
            phoneNO = orderData!.restaurant_number!
        }
        else
        {
            phoneNO = orderData!.customer_number!
        }
        
        if let url = URL(string: "tel://\(String(describing: phoneNO))"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    @IBAction func onClickDone(_ sender: UIButton) {
        setOrderComplete(orderID: "\(orderId)")
    }
    
    
}
extension OrderDetailsVC:FlexibleSteppedProgressBarDelegate{
    func progressBar(_ progressBar: FlexibleSteppedProgressBar,
                 didSelectItemAtIndex index: Int) {
        print("Index selected!")
    }
    
    func progressBar(_ progressBar: FlexibleSteppedProgressBar,
                 willSelectItemAtIndex index: Int) {
        print("Index selected!")
    }

    func progressBar(_ progressBar: FlexibleSteppedProgressBar,
                     canSelectItemAtIndex index: Int) -> Bool {
        return true
    }

    func progressBar(_ progressBar: FlexibleSteppedProgressBar,
                     textAtIndex index: Int, position: FlexibleSteppedProgressBarTextLocation) -> String {
        if position == FlexibleSteppedProgressBarTextLocation.bottom {
            switch index {
                
            case 0: return "Order being Prepared"
            case 1: return "Ready for Pickup"
            case 2: return "Order Dispatched"
            default: return ""
                
            }
        }
    return ""
    }
}

extension OrderDetailsVC : UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if orderData != nil {
            let products = orderData!.products!
            return products.count != 0 ? products.count:0
    }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "itemCell")
        
        let imgview:UIImageView = cell?.viewWithTag(201) as! UIImageView
        let lblPName:UILabel = cell?.viewWithTag(202) as! UILabel
        let lblPextra:UILabel = cell?.viewWithTag(203) as! UILabel
        let lblPPrice:UILabel = cell?.viewWithTag(204) as! UILabel
        imgview.sd_setImage(with: URL(string: orderData!.products![indexPath.row].product_images!), completed: nil)
        
        lblPName.text = orderData!.products![indexPath.row].product_name
        lblPextra.text = orderData!.products![indexPath.row].ingredientData!.count > 0 ? orderData!.products![indexPath.row].ingredientData![0].ingredient_name! : " "
        lblPPrice.text = "$\(orderData!.products![indexPath.row].product_price!)"
        
        return cell!
    }
    
    
/*    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
       ConstantTblViewHeight.constant = 108 * 10
        ConstantViewHeight.constant = 250 + ConstantTblViewHeight.constant
        ConstantViewMainHeight.constant = 271 + ConstantViewHeight.constant
        scrollView.contentSize.height =  180 + ConstantViewMainHeight.constant
    }*/
}
//MARK:- API CALL
extension OrderDetailsVC{
    private func getOrderDetailsNoLoading(){
        print("No Loading GetorderDetails:-\(Date())")
        var parameter = [String: Any]()
        let runnerId = USERDEFAULT.value(forKey: Kuser_id)
        parameter = ["runner_id":runnerId ?? "0","order_id":"\(orderId)"] as [String : Any]
        debugPrint(parameter)
        
        NetworkManager.shared.apiCallWithTokenNoLoading(Api: ORDERDETAILSRUNNER_URL, param: parameter , getView: self) { (response) in
            if let getdata = response as? NSDictionary {
                print(getdata)
                
                if let data = getdata.value(forKey: DATA) as? String{
                    let DES = CryptoJS.AES()
                    let decryptedString = DES.decrypt(data, password: "rswna0hu8t")
                    print(decryptedString)
                    
                    let DecryptedData = decryptedString.data(using: .utf8)!
                    do{
                        let ModelData = try JSONDecoder().decode(OrderDetailsModel.self, from: DecryptedData)
                      print(ModelData)
                        self.setDetails(data: ModelData.getData!)
                      /*  self.arrAcceptOrderList = ModelData

                        print(self.arrAcceptOrderList as Any)
                        
                        self.tblViewListing.reloadData()*/
                    }catch{
                        debugPrint(error.localizedDescription)
                    }
                } else {
                    print("No Data")
                }
            } else {
                print("Response is not in json")
            }
        }
    }
    
    private func getOrderDetails(){
        print("GetorderDetails:-\(Date())")
        var parameter = [String: Any]()
        let runnerId = USERDEFAULT.value(forKey: Kuser_id)
        parameter = ["runner_id":runnerId ?? "0","order_id":"\(orderId)"] as [String : Any]
        debugPrint(parameter)
        
        NetworkManager.shared.apiCallWithToken(Api: ORDERDETAILSRUNNER_URL, param: parameter , getView: self) { (response) in
            if let getdata = response as? NSDictionary {
                print(getdata)
                
                if let data = getdata.value(forKey: DATA) as? String{
                    let DES = CryptoJS.AES()
                    let decryptedString = DES.decrypt(data, password: "rswna0hu8t")
                    print(decryptedString)
                    
                    let DecryptedData = decryptedString.data(using: .utf8)!
                    do{
                        let ModelData = try JSONDecoder().decode(OrderDetailsModel.self, from: DecryptedData)
                      print(ModelData)
                        self.setDetails(data: ModelData.getData!)
                      /*  self.arrAcceptOrderList = ModelData

                        print(self.arrAcceptOrderList as Any)
                        
                        self.tblViewListing.reloadData()*/
                    }catch{
                        debugPrint(error.localizedDescription)
                    }
                } else {
                    print("No Data")
                }
            } else {
                print("Response is not in json")
            }
        }
    }
    private func setOrderComplete(orderID: String){
        var parameter = [String: Any]()
        let runnerId = USERDEFAULT.value(forKey: Kuser_id)
        parameter = ["id": orderID,"order_status":"5"] as [String : Any]
        debugPrint(parameter)
        
        NetworkManager.shared.apiCallWithToken(Api: RUNNERCOMPLETESTATUS_URL, param: parameter , getView: self) { (response) in
            if let getdata = response as? NSDictionary {
                print(getdata)
                
                if getdata.value(forKey: "success_code") as? Int == 200{
                    self.moveToOrderComplete()
                } else {
                    print("No Data")
                }
            } else {
                print("Response is not in json")
            }
        }
    }
    func moveToOrderComplete()
    {
        let vc = storyboard?.instantiateViewController(withIdentifier: "OrderCompleteVC")as! OrderCompleteVC
        vc.orderiD = orderId
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func setDetails(data:[GetDatum]){
        orderData = data[0]
        flexStepper.numberOfPoints = 3
        flexStepper.delegate = self
        
        let resdata = data[0]
        switch resdata.order_status! {
        case 2:
            orderstatus = .Prepare
            flexStepper.currentIndex = 0
            btnDone.isHidden = true
            btnDoneHeightConstraint.constant = 0
            lblCallRestaurent.text = "Call Restaurant"
        case 3:
            orderstatus = .Ready
            btnDone.isHidden = true
            flexStepper.currentIndex = 1
            btnDoneHeightConstraint.constant = 0
            lblCallRestaurent.text = "Call Restaurant"
        case 4:
            orderstatus = .Dispatched
            flexStepper.currentIndex = 2
            btnDone.isHidden = false
            btnDoneHeightConstraint.constant = 45
            lblCallRestaurent.text = "Call Customer"
            timer.invalidate()
        case 5:
            orderstatus = .Dispatched
            flexStepper.currentIndex = 2
            btnDone.isHidden = true
            btnDoneHeightConstraint.constant = 0
            lblCallRestaurent.text = "Call Customer"
            timer.invalidate()
        default:
            print("default")
            btnDone.isHidden = true
        }
        
        if let dateStr = resdata.created_date{
            let dateFormatter = DateFormatter()
            dateFormatter.locale = Locale(identifier: "en_US_POSIX") // set locale to reliable US_POSIX

            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.sssZ"
            
            if let date = dateFormatter.date(from:dateStr){
                dateFormatter.dateFormat = "HH:mm"//"dd MM YYYY HH:mm:ss"
//
                
                let dateAsString = dateFormatter.string(from: date)
               
                dateFormatter.dateFormat = "HH:mm"

                let dated = dateFormatter.date(from: dateAsString)
                dateFormatter.dateFormat = "h:mm a"
                let date12 = dateFormatter.string(from: dated!)
                //print("12 hour formatted Date:",date12)
                //print("\(indexPath.row).\(data!.created_date!)")
                lblOrderTIme.text = date12//JSON(date.timeAgoSinceNow as Any).stringValue

            }else{
                debugPrint("Error")
  //              cell.lblTime.text = ""
                lblOrderTIme.text = ""
            }
        }else{
            debugPrint("Error")
            lblOrderTIme.text = ""
        }
        
        var profileImgUrl = resdata.restaurant_image != nil ? resdata.restaurant_image : ""
        var resname = resdata.restaurant_name!
        var resAdd = resdata.restaurant_address!
        
        switch orderstatus {
        case .Dispatched:
            profileImgUrl = resdata.user_image != nil ? resdata.user_image! : ""
            resname = "\(resdata.first_name! ?? "")  \(resdata.last_name! )"
            resAdd = "\(resdata.floor_name ?? "0") \(resdata.row ?? "") \(resdata.seat ?? "")"
        case .Prepare,.Ready:
            print("as it is Restaurent Details.")
        }
        
        lblResName.text = resname
        lblAddress.text = resAdd

        //2021-08-18T12:29:54.000Z
        imgRes.sd_setImage(with: URL(string: profileImgUrl!), placeholderImage: UIImage(named: "ic_SideMenu_1"), options:.highPriority, context: nil)
        lblCustomerName.text = "\(resdata.first_name! ) \(resdata.last_name! )"
        lblCustomerContactNo.text = "\(resdata.country_code != nil ? resdata.country_code! : "") \(resdata.restaurant_number != nil ? resdata.restaurant_number! :"")"
        lblOrderNo.text = "Order No:- \(resdata.order_id! )"
        lblResNameDetail.text = resdata.restaurant_name!
        lblCustAddress.text = "\(resdata.floor_name ?? "0") \(resdata.row ?? "") \(resdata.seat ?? "")"
        lblItemsCount.text = "Item(\(resdata.products!.count ))"
        lblTotalAmount.text = "Total $\(resdata.total_amount! )"
        if isTblReloaded == false{
            isTblReloaded = true
            tblViewItem.reloadData()
        }
    }
}

enum orderStatus {
    case Prepare
    case Ready
    case Dispatched
}
// MARK: - Welcome
struct OrderDetailsModel : Decodable{
    let getData: [GetDatum]?
}

// MARK: - GetDatum
struct GetDatum : Decodable{
    let order_id, order_status: Int?
    let created_date, updated_date: String?
    let total_amount: Double?
    let first_name, last_name, country_code, customer_number, restaurant_number: String?
    let user_image: String?
    let restaurant_name, restaurant_address: String?
    let restaurant_image: String?
    let floor_id: Int?
    let floor_name: String?
    let section_id: Int?
    let section_name: String?
    let row_id: Int?
    let row: String?
    let seat_id: Int?
    let seat: String?
    let products: [ProductD]?
}

// MARK: - Product
struct ProductD : Decodable{
    let quantity, product_ingredient_id: Int?
    let product_size_id, product_color_id: Int?
    let product_id: Int?
    let type, product_name: String?
    let product_price: Int?
    let disscount_price: String?
    let product_images: String?
    let ingredientData: [IngredientDatum]?
}

// MARK: - IngredientDatum
struct IngredientDatum : Decodable{
    let id: Int?
    let product_id: String?
    let type: Int?
    let ingredient_name, price: String?
}

/*
 "quantity": 1,
                         "product_ingredient_id": null,
                         "product_size_id": null,
                         "product_color_id": null,
                         "product_id": 4,
                         "type": "0",
                         "product_name": "Seven Cheese",
                         "product_price": 15,
                         "disscount_price": "14.55",
                         "product_images": "https://stadium.siyadev.online:3005/pizza.jpg",
                         "ingredientData": [
                             {
                                 "id": 25,
                                 "product_id": "4",
                                 "type": 1,
                                 "ingredient_name": "Extra cheese",
                                 "price": "10"
                             },
                             {
                                 "id": 26,
                                 "product_id": "4",
                                 "type": 2,
                                 "ingredient_name": "butter",
                                 "price": "10"
                             },
                             {
                                 "id": 27,
                                 "product_id": "4",
                                 "type": 2,
                                 "ingredient_name": "Extra cheese",
                                 "price": "10"
                             },
                             {
                                 "id": 28,
                                 "product_id": "4",
                                 "type": 3,
                                 "ingredient_name": "butter",
                                 "price": "10"
                             },
                             {
                                 "id": 29,
                                 "product_id": "4",
                                 "type": 3,
                                 "ingredient_name": "Extra cheese",
                                 "price": "10"
                             }
                         ]
                     }
 */
             /*  "total_amount": 15.3,
               "first_name": "rajdeep",
               "last_name": "zala",
               "country_code": "+91",
               "mobile": "9725874716",
               "user_image": null,
               "restaurant_name": "Bobby Nick's Grilllll",
               "restaurant_address": "Vastrapur.",
               "restaurant_image": "https://stadium.siyadev.online:3005/bobby_nick's.jpg",
               "floor_id": 5,
               "floor_name": "First",
               "section_id": 6,
               "section_name": "gs",
               "row_id": 50,
               "row": "Gs-1",
               "seat_id": 68,
               "seat": "1",
    
 */


