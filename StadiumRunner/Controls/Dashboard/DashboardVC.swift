//
//  DashboardVC.swift
//  StadiumRunner
//
//  Created by XcelTec-053 on 02/04/21.
//  Copyright © 2021 XcelTec-053. All rights reserved.
//

import UIKit

class DashboardVC: SuperViewController,UITextViewDelegate {
    
    @IBOutlet var viewDeclinePopup: UIView!
    @IBOutlet weak var tblViewListing: UITableView!
    @IBOutlet weak var lblHeaderTitle: UILabel!
    @IBOutlet weak var lblAccepted: UILabel!
    @IBOutlet weak var lblNewOrder: UILabel!
    @IBOutlet weak var viewAcceptedList: UIView!
    @IBOutlet weak var viewOrderList: UIView!
    
    var isNewOrder : Bool = true
    var arrAcceptOrderList = NewOrderData()
    var arrNewOrderList = NewOrderData()
    var cancelOrderID = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        txtReason.text = "Write The Reason"
        txtReason.textColor = UIColor.lightGray
        txtReason.delegate = self
        getNewOrderData()
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Write The Reason"
            textView.textColor = UIColor.lightGray
        }
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    @IBAction func btnViewAllAction(_ sender: UIButton) {
        
    }
    
    @IBAction func onClickNotification(_ sender: UIButton) {
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "NotificationVC") as! NotificationVC
        self.navigationController?.pushViewController(vc, animated: true)

        
//        let vc = storyboard?.instantiateViewController(withIdentifier: "MyProfileVC") as! MyProfileVC
//        self.navigationController?.pushViewController(vc, animated: true)

        //        let vc = storyboard?.instantiateViewController(withIdentifier: "MyOrderListVC") as! MyOrderListVC
//        self.navigationController?.pushViewController(vc, animated: true)

    }
    
    @IBAction func btnSideMenuActions(_ sender: UIButton) {
        self.slideMenuController()?.openLeft()
    }
    
    @IBAction func btnOrderActions(_ sender: UIButton) {
        viewOrderList.backgroundColor = #colorLiteral(red: 0.9215686275, green: 0.1215686275, blue: 0.231372549, alpha: 1)
        viewAcceptedList.backgroundColor = .clear
        isNewOrder = true
        lblAccepted.textColor = #colorLiteral(red: 0.9215686275, green: 0.1215686275, blue: 0.231372549, alpha: 1)
        lblNewOrder.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        lblNewOrder.font = UIFont(name: "Segoe UI Bold", size: 14.0)
        lblAccepted.font = UIFont(name: "Segoe UI", size: 14.0)
        lblHeaderTitle.text = "Order List"
        tblViewListing.reloadData()
        getNewOrderData()
    }
    
    @IBAction func btnAcceptedACtions(_ sender: UIButton) {
        viewOrderList.backgroundColor = .clear
        viewAcceptedList.backgroundColor = #colorLiteral(red: 0.9215686275, green: 0.1215686275, blue: 0.231372549, alpha: 1)
        isNewOrder = false
        lblNewOrder.textColor = #colorLiteral(red: 0.9215686275, green: 0.1215686275, blue: 0.231372549, alpha: 1)
        lblAccepted.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        lblAccepted.font = UIFont(name: "Segoe UI Bold", size: 14.0)
        lblNewOrder.font = UIFont(name: "Segoe UI", size: 14.0)
        lblHeaderTitle.text = "Accepted Order List"
        tblViewListing.reloadData()
        getAcceptedOrderList()
    }
    
    @IBOutlet weak var txtReason: UITextView!
    
    @IBAction func onClickDoneReject(_ sender: UIButton) {
        declineOrderAPI(sender: UIButton())
    }
    
    
    @IBAction func onClickCancel(_ sender: UIButton) {
        self.viewDeclinePopup.alpha = 0
    }
}

//MARK:- API Call
extension DashboardVC{
    
    private func getNewOrderData(){
        var parameter = [String: Any]()
        let runnerId = USERDEFAULT.value(forKey: Kuser_id)
        parameter = ["runner_id":runnerId ?? "0"] as [String : Any]
        debugPrint(parameter)
        
        NetworkManager.shared.apiCallWithToken(Api: NEWORDER_URL, param: parameter , getView: self) { (response) in
            if let getdata = response as? NSDictionary {
                print(getdata)
                
                if let data = getdata.value(forKey: DATA) as? String{
                    let DES = CryptoJS.AES()
                    let decryptedString = DES.decrypt(data, password: "rswna0hu8t")
                    print(decryptedString)
                    
                    let DecryptedData = decryptedString.data(using: .utf8)!
                    do{
                        let ModelData = try JSONDecoder().decode(NewOrderData.self, from: DecryptedData)
                        self.arrNewOrderList = ModelData

                        print(self.arrAcceptOrderList as Any)
                        
                        self.tblViewListing.reloadData()
                    }catch{
                        debugPrint(error.localizedDescription)
                    }
                } else {
                    self.arrNewOrderList.getData?.removeAll()
                    self.tblViewListing.reloadData()
                    print("No Data")
                }
            } else {
                print("Response is not in json")
            }
        }
    }
    
    private func getAcceptedOrderList(){
        var parameter = [String: Any]()
        let runnerId = USERDEFAULT.value(forKey: Kuser_id)
        parameter = ["runner_id":runnerId ?? "0"] as [String : Any]
        debugPrint(parameter)
        
        NetworkManager.shared.apiCallWithToken(Api: ACCEPTED_ORDER_URL, param: parameter , getView: self) { (response) in
            if let getdata = response as? NSDictionary {
                print(getdata)
                
                if let data = getdata.value(forKey: DATA) as? String{
                    let DES = CryptoJS.AES()
                    let decryptedString = DES.decrypt(data, password: "rswna0hu8t")
                    print(decryptedString)
                    
                    let DecryptedData = decryptedString.data(using: .utf8)!
                    
                    do{
                        let ModelData = try JSONDecoder().decode(NewOrderData.self, from: DecryptedData)
                        self.arrAcceptOrderList = ModelData

                        print(self.arrAcceptOrderList as Any)
                        
                        self.tblViewListing.reloadData()
                    }catch{
                        debugPrint(error.localizedDescription)
                    }
                } else {
                    print("No Data")
                }
            } else {
                print("Response is not in json")
            }
        }
    }
    @objc func declineOrderAPI(sender:UIButton){
        
        var parameter = [String: Any]()
        let runnerId = USERDEFAULT.value(forKey: Kuser_id)
        parameter = ["runner_id":runnerId ?? "0",
                     "order_id":"\(cancelOrderID)","reason":txtReason.text!] as [String : Any]
        debugPrint(parameter)
        self.onClickCancel(UIButton())
        NetworkManager.shared.apiCallWithToken(Api: REJECTORDER_URL, param: parameter , getView: self) { (response) in
            if let getdata = response as? NSDictionary {
                print(getdata)
                self.cancelOrderID = 0
                if getdata["success_code"] as! Int == 200 {
                    self.getNewOrderData()
                } else {
                    print("No Data")
                }
//                if let data = getdata.value(forKey: DATA) as? String{
//                    let DES = CryptoJS.AES()
//                    let decryptedString = DES.decrypt(data, password: "rswna0hu8t")
//                    print(decryptedString)
//
//                    let DecryptedData = decryptedString.data(using: .utf8)!
//                    do{
//                        let output = try JSONSerialization.jsonObject(with: DecryptedData, options: .allowFragments)
//
//                        print(output as Any)
////                        let restaurant = RestaurantDetail(fromJson: JSON(output as Any).arrayValue.last)
//
//
////                        self.arrNewOrderList.getData!.removeAll()
////                        self.btnOrderActions(UIButton())
//                    }catch{
//                        debugPrint(error.localizedDescription)
//                    }
//                } else {
//                }
            } else {
                print("Response is not in json")
            }
        }
    }
    @objc func acceptOrderAPI(sender:UIButton){
        var parameter = [String: Any]()
        let runnerId = USERDEFAULT.value(forKey: Kuser_id)
        parameter = ["runner_id":runnerId ?? "0",
                     "order_id":"\(sender.tag)"] as [String : Any]
        debugPrint(parameter)
        
        NetworkManager.shared.apiCallWithToken(Api: ACCEPTORDERLIST_URL, param: parameter , getView: self) { (response) in
            if let getdata = response as? NSDictionary {
                print(getdata)
                
                if let data = getdata.value(forKey: DATA) as? String{
                    let DES = CryptoJS.AES()
                    let decryptedString = DES.decrypt(data, password: "rswna0hu8t")
                    print(decryptedString)
                    
                    let DecryptedData = decryptedString.data(using: .utf8)!
                    do{
                        let output = try JSONSerialization.jsonObject(with: DecryptedData, options: .allowFragments)
                        
                        print(output as Any)
//                        let restaurant = RestaurantDetail(fromJson: JSON(output as Any).arrayValue.last)
                        self.getNewOrderData()
                    }catch{
                        debugPrint(error.localizedDescription)
                    }
                } else {
                    print("No Data")
                }
            } else {
                print("Response is not in json")
            }
        }
    }
}

extension DashboardVC : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if isNewOrder {
            if let count = arrNewOrderList.getData?.count {
                if count == 0 {
                    tblViewListing.setEmptyView(title: "", message: "New order not available", messageImage: UIImage())
                    return 0
                }
                tblViewListing.setEmptyView(title: "", message: "", messageImage: UIImage())
                return count
            } else {
                tblViewListing.setEmptyView(title: "", message: "New order not available", messageImage: UIImage())
                return 0
            }
        } else {
            if let count = arrAcceptOrderList.getData?.count {
                if count == 0 {
                    tblViewListing.setEmptyView(title: "", message: "Accept order not available", messageImage: UIImage())
                    return 0
                }
                tblViewListing.setEmptyView(title: "", message: "", messageImage: UIImage())
                return count
            } else {
                tblViewListing.setEmptyView(title: "", message: "Accept order not available", messageImage: UIImage())
                return 0
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "OrderCellClass") as! OrderCellClass

        var data : GetData?
        
        if isNewOrder == true {
            data = arrNewOrderList.getData?[indexPath.row]
            cell.ConstantViewHeight.constant = 30
        }else{
            data = arrAcceptOrderList.getData?[indexPath.row]
            cell.ConstantViewHeight.constant = 0
        }

        var strProducts: String?
        for element in data?.products?.flatMap({$0.product_name}) ?? [""] {
            if strProducts == nil {
                strProducts = element
            } else {
                strProducts = strProducts! + ", " + element
            }
        }

        cell.lblName.text = "\(data?.first_name ?? "") \(data?.last_name ?? "")"
        cell.lblFoodDetails.text = "Order Id : \(data?.order_id ?? 1)\n\(strProducts ?? "")"
        cell.lblRestarantName.text = data?.restaurant_name ?? ""
        cell.lblTblName.text = "\(data?.floor_name ?? "0") \(data?.row ?? "") \(data?.seat ?? "")"
        cell.lblTime.text = String.getFormattedDate(string: data?.created_date ?? "", formatter: "HH:mm a")

        cell.btnAccepted.tag = data?.order_id ?? 1
        cell.btnAccepted.addTarget(self, action: #selector(acceptOrderAPI(sender:)), for: .touchUpInside)
        cell.btnDecline.tag = data?.order_id ?? 1
        cell.btnDecline.addTarget(self, action: #selector(btnDeclineActions), for: .touchUpInside)


        if let dateStr = data?.created_date!{
            let dateFormatter = DateFormatter()
            dateFormatter.locale = Locale(identifier: "en_US_POSIX") // set locale to reliable US_POSIX

            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.sssZ"
            
            if let date = dateFormatter.date(from:dateStr){
                dateFormatter.dateFormat = "HH:mm"//"dd MM YYYY HH:mm:ss"
//
                
                let dateAsString = dateFormatter.string(from: date)
               
                dateFormatter.dateFormat = "HH:mm"

                let dated = dateFormatter.date(from: dateAsString)
                dateFormatter.dateFormat = "h:mm a"
                let date12 = dateFormatter.string(from: dated!)
                //print("12 hour formatted Date:",date12)
                //print("\(indexPath.row).\(data!.created_date!)")
                cell.lblTime.text = date12//JSON(date.timeAgoSinceNow as Any).stringValue

            }else{
                debugPrint("Error")
  //              cell.lblTime.text = ""
                cell.lblTime.text = ""
            }
        }else{
            debugPrint("Error")
            cell.lblTime.text = ""
        }

        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if isNewOrder == true {
           // data = arrNewOrderList.getData?[indexPath.row]
            // New order
        }else{
            let vc = storyboard?.instantiateViewController(withIdentifier: "OrderDetailsVC") as! OrderDetailsVC
            vc.orderId = (arrAcceptOrderList.getData?[indexPath.row].order_id)!
            vc.comefrom = .dashboard
            self.navigationController?.pushViewController(vc, animated: true)
            // Goto ORder Tracking Screen.
        }
    }
    @objc func btnAcceptedActions(sender: UIButton) {
        print(sender.tag)
    }
    @objc func btnDeclineActions(sender: UIButton) {
        print(sender.tag)
        cancelOrderID = sender.tag
        self.viewDeclinePopup.frame = self.view.frame
        self.viewDeclinePopup.alpha = 1
        self.view.addSubview(self.viewDeclinePopup)
    }
}

class OrderCellClass: UITableViewCell {
    @IBOutlet weak var ConstantViewHeight: NSLayoutConstraint!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblFoodDetails: UILabel!
    @IBOutlet weak var lblRestarantName: UILabel!
    @IBOutlet weak var lblTblName: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var btnAccepted: UIButton!
    @IBOutlet weak var btnDecline: UIButton!
}

