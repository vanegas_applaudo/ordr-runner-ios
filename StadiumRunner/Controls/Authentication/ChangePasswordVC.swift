//
//  ChangePasswordVC.swift
//  StadiumUser
//
//  Created by XcelTec-053 on 30/03/21.
//  Copyright © 2021 XcelTec-053. All rights reserved.
//

import UIKit

class ChangePasswordVC: SuperViewController {

    @IBOutlet weak var txtConfirmPassword: UITextField!
    @IBOutlet weak var txtNewPassword: UITextField!
    @IBOutlet weak var txtOldPassword: UITextField!
    
    @IBOutlet weak var btnShowHideOldPassword: UIButton!
    @IBOutlet weak var btnShowHideNewPassword: UIButton!
    @IBOutlet weak var btnShowHideConfirmPassword: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
   
    @IBAction func onClickBAck(_ sender: UIButton) {
        Helper.sharedHelper.goToHome()
    }
    @IBAction func btnChangePasswordActions(_ sender: UIButton) {
       checkValidation()
    }
    
    @IBAction func btnHideShowOldPasswordAction(_ sender: UIButton) {
        if(btnShowHideOldPassword.imageView?.image == UIImage (named: "ic_hidePasword"))
        {
            txtOldPassword.isSecureTextEntry = false
            btnShowHideOldPassword.setImage(UIImage (named: "ic_passwordShow"), for: .normal)
        }
        else
        {
            txtOldPassword.isSecureTextEntry = true
            btnShowHideOldPassword.setImage(UIImage (named: "ic_hidePasword"), for: .normal)
        }
    }
    
    @IBAction func btnHideShowNewPasswordAction(_ sender: UIButton) {
        if(btnShowHideNewPassword.imageView?.image == UIImage (named: "ic_hidePasword"))
        {
            txtNewPassword.isSecureTextEntry = false
            btnShowHideNewPassword.setImage(UIImage (named: "ic_passwordShow"), for: .normal)
        }
        else
        {
            txtNewPassword.isSecureTextEntry = true
            btnShowHideNewPassword.setImage(UIImage (named: "ic_hidePasword"), for: .normal)
        }
    }
    
    @IBAction func btnHideShowConfirmPasswordAction(_ sender: UIButton) {
        if(btnShowHideConfirmPassword.imageView?.image == UIImage (named: "ic_hidePasword"))
        {
            txtConfirmPassword.isSecureTextEntry = false
            btnShowHideConfirmPassword.setImage(UIImage (named: "ic_passwordShow"), for: .normal)
        }
        else
        {
            txtConfirmPassword.isSecureTextEntry = true
            btnShowHideConfirmPassword.setImage(UIImage (named: "ic_hidePasword"), for: .normal)
        }
    }
    
    //MARK:- Validation check
     
     func checkValidation()
     {
        txtOldPassword.text = txtOldPassword.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        txtNewPassword.text = txtNewPassword.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        txtConfirmPassword.text = txtConfirmPassword.text?.trimmingCharacters(in: .whitespacesAndNewlines)

        ToaststyleRed.backgroundColor = #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1)
         ToaststyleGreen.backgroundColor = #colorLiteral(red: 0, green: 0.5882352941, blue: 0.2509803922, alpha: 1)
        if(txtOldPassword.text == ""){
            self.view.makeToast(msgEmptyOldPassword, duration: TOAST_TIME, position: .bottom,style:ToaststyleRed)
        }else if(txtNewPassword.text == ""){
             self.view.makeToast(msgEmptyNewPassword, duration: TOAST_TIME, position: .bottom,style:ToaststyleRed)
         }else if(txtConfirmPassword.text == ""){
             self.view.makeToast(msgConfirmPassword, duration: TOAST_TIME, position: .bottom,style:ToaststyleRed)
         }else if(txtConfirmPassword.text != txtNewPassword.text){
             self.view.makeToast(msgSamePassword, duration: TOAST_TIME, position: .bottom,style:ToaststyleRed)
         }else if(Helper.sharedHelper.passwordValidation(txtOldPassword.text! as NSString) == false) {
             self.view.makeToast(msgPasswordMin, duration: TOAST_TIME, position: .bottom,style:ToaststyleRed)
         }else if(Helper.sharedHelper.passwordValidation(txtNewPassword.text! as NSString) == false) {
             self.view.makeToast(msgPasswordMin, duration: TOAST_TIME, position: .bottom,style:ToaststyleRed)
         }else if(txtOldPassword.text == txtNewPassword.text){
            self.view.makeToast(msgOldNewSamePassword, duration: TOAST_TIME, position: .bottom,style:ToaststyleRed)
         }
         else{
             APIChangePassword()
         }
     }
    
    //MARK:- API call
    func APIChangePassword()
    {
        let parameter = [kNewPassword:txtNewPassword.text!,
                         kOldPassword:txtOldPassword.text!]
        print(parameter)
        
        NetworkManager.shared.apiCallWithToken(Api: CHANGEPASS_URL, param: parameter, getView: self) { (response) in
            if let getdata = response as? NSDictionary
            {
                print(getdata)
                let model = MainModelClass.init(data: getdata)
                if model.code == 200
                {
                    self.view.makeToast(model.message, duration: 2.0, position: .bottom,style:ToaststyleGreen)

                    self.goLogin()
                }else if model.code == 400 || model.code == 401 || model.code == 403   {
                    self.goToLogin()
                }else{
                    self.view.makeToast(model.message, duration: TOAST_TIME, position: .bottom,style:ToaststyleRed)
                }
            }else{
                self.view.makeToast(msgSomethingWentWrong, duration: TOAST_TIME, position: .bottom,style:ToaststyleRed)
            }
        }
    }
    
    func goLogin(){
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
            Helper.sharedHelper.goToLogin()
        }
    }
}
