//
//  LoginVC.swift
//  StadiumRunner
//
//  Created by XcelTec-053 on 02/04/21.
//  Copyright © 2021 XcelTec-053. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD
import CountryPickerView
//var fcmTocken = ""
class LoginVC: SuperViewController {

    @IBOutlet weak var viewEmail: UIView!
    @IBOutlet weak var btnRememberMe: UIButton!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtemail: UITextField!
    
    var isEmailSelected:Bool = true
    var loginData : LoginModelData!
    var userEmail: String = String()
    var firstName: String = String()
    var lastName: String = String()
    var FullName: String = String()
    var appID: String = String()
    var Image: String = String()
    var country = CountryPickerView(frame: CGRect(x: 0, y: 0, width: 300, height: 700))
    var getCountryCode = String()
    var getCountryName = String()

    override func viewDidLoad() {
        super.viewDidLoad()
//        txtemail.text = "johndoe@mail.com"
//        txtPassword.text = "Password@123"
        txtPassword.isSecureTextEntry = true

    }
    
    
    //MARK:- User defined function
    func checkValidation()
    {
        ToaststyleRed.backgroundColor = #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1)
        ToaststyleGreen.backgroundColor = #colorLiteral(red: 0, green: 0.5882352941, blue: 0.2509803922, alpha: 1)
        
        if(txtemail.text == "")
        {
            self.view.makeToast(msgEmail, duration: TOAST_TIME, position: .bottom,style:ToaststyleRed)
        }
        else if(txtPassword.text == "")
        {
            self.view.makeToast(msgPassword, duration: TOAST_TIME, position: .bottom,style:ToaststyleRed)
        }
        else
        {
            //self.goToHome()
            APILogin()
        }
    }
    
    //MARK:- All Button Actions
    
    @IBAction func btnRemberAction(_ sender: UIButton) {
        if btnRememberMe.currentImage == UIImage(named: "ic_uncheck") {
            btnRememberMe.setImage(#imageLiteral(resourceName: "ic_check"), for: .normal)
            USERDEFAULT.set(txtemail.text!, forKey: "email")
            USERDEFAULT.set(txtPassword.text!, forKey: "password")
        }else{
            btnRememberMe.setImage(#imageLiteral(resourceName: "ic_uncheck"), for: .normal)
            USERDEFAULT.removeObject(forKey: "email")
            USERDEFAULT.removeObject(forKey: "password")
        }

    }
    
    @IBAction func btnForgotPassoword(_ sender: UIButton) {
        Helper.sharedHelper.forgotPwd()
    }
    
    @IBAction func btnPasswordHideShowAction(_ sender: UIButton) {
        if(sender.imageView?.image == UIImage (named: "ic_passwordShow"))
        {
            sender.setImage(UIImage (named: "ic_hidePasword"), for: .normal)
            txtPassword.isSecureTextEntry = true
        }
        else
        {
            sender.setImage(UIImage (named: "ic_passwordShow"), for: .normal)
            txtPassword.isSecureTextEntry = false
        }
    }
    
    @IBAction func BtnLoginAction(_ sender: UIButton) {
        checkValidation()
    }
}

//MARK:- All API Call

extension LoginVC {
        
    func APILogin()
    {
      var fcmToken = kFcmToken
      if let token = USERDEFAULT.value(forKey: "fcmtoken") as? String{
          fcmToken = token
      }
     // fcmToken = fcmTocken
      let parameter = [Kdevice_type:1,
                       Kuser_device_id:UDID,
                       Kfcm_token:fcmToken,
                       Kuser_type:0,
                       Kemail:txtemail.text!,
                       kPassword:txtPassword.text!] as [String : Any]
      print(parameter)
      NetworkManager.shared.apiCall(Api: LOGIN_URL, param: parameter , getView: self) { (response) in
          if let getdata = response as? NSDictionary {
              print(getdata)
              let model = LoginModelClass.init(fromDictioanry: getdata)
              if model.code == 1 {
                  self.loginData = model.data
                  USERDEFAULT.set(self.loginData.contact_number, forKey: KcontactNumber)
                  USERDEFAULT.set(self.loginData.country_code, forKey: Kcountry_code)
                  USERDEFAULT.set(self.loginData.country_code_name, forKey: Kcountry_code_name)
                  USERDEFAULT.set(self.loginData.email, forKey: Kemail)
                 // USERDEFAULT.set(self.loginData.full_name, forKey: KUser_name)
                  USERDEFAULT.set(self.loginData.token, forKey: kAccesstoken)
                  USERDEFAULT.set(self.loginData.user_id, forKey: Kuser_id)
                  USERDEFAULT.set(self.loginData.user_image, forKey: Kimage)
                  USERDEFAULT.set(self.loginData.user_type, forKey: Kuser_type)
                  USERDEFAULT.set(true, forKey: "isLoggedIn")
                  self.view.makeToast(model.message, duration: TOAST_TIME, position: .bottom,style:ToaststyleGreen)

                  if self.btnRememberMe.currentImage == UIImage(named: "ic_check") {
                      USERDEFAULT.set(self.txtemail.text!, forKey: "email")
                      USERDEFAULT.set(self.txtPassword.text!, forKey: "password")
                  }

                  DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                    Helper.sharedHelper.goToHome()
                  }
              } else if   model.code == 400 || model.code == 401 || model.code == 403  {
                  self.goToLogin()
              } else{
                  self.view.makeToast(model.message, duration: TOAST_TIME, position: .bottom,style:ToaststyleRed)
              }
          } else {
              print("Response is not in json")
          }
      }
    }
}

