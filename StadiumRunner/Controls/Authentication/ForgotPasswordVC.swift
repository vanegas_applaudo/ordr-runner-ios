//
//  ForgotPasswordVC.swift
//  StadiumUser
//
//  Created by Xceltec-071 on 24/03/21.
//  Copyright © 2021 XcelTec-053. All rights reserved.
//

import UIKit

class ForgotPasswordVC: SuperViewController{

    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var btnResetPassword: UIButton!
    @IBOutlet weak var txtEmailID: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    //MARK:- User defined methods
    func checkValidation()
    {
        ToaststyleRed.backgroundColor = #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1)
        ToaststyleGreen.backgroundColor = #colorLiteral(red: 0, green: 0.5882352941, blue: 0.2509803922, alpha: 1)
        
        if txtEmailID.text == ""
        {
            self.view.makeToast(msgEmail, duration: TOAST_TIME, position: .bottom,style:ToaststyleRed)
        }else if (Helper.sharedHelper.validateEmailWithString(txtEmailID.text! as NSString) == false) {
            self.view.makeToast(msgCorrectEmail, duration: TOAST_TIME, position: .bottom,style:ToaststyleRed)
        }
        else
        {
            APIForgotPassword(email: txtEmailID.text!)
            
            // APIForgotPassword()
        }
    }
    
    //MARK:- All Button Actions
    
    @IBAction func btnResetPasswordAction(_ sender: UIButton) {
        checkValidation()
    }
    
    @IBAction func btnLoginAction(_ sender: UIButton) {
//        self.navigationController?.popViewController(animated: true)
        Helper.sharedHelper.goToLogin()
    }
    func goBAck(){
        DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
            self.btnLoginAction(UIButton())
        }
    }
    
     func APIForgotPassword(email:String){
        var parameter = [String: Any]()
        parameter = ["user_type":0,"email":email] as [String : Any]
        debugPrint(parameter)
        
        NetworkManager.shared.apiCallWithToken(Api: FORGOTPASS_URL, param: parameter , getView: self) { (response) in
            if let getdata = response as? NSDictionary {
                print(getdata)
                let success = getdata["success"] as! Int
                let strMsg =  getdata["message"] as! String
                self.view.makeToast(strMsg, duration: TOAST_TIME, position: .bottom,style:ToaststyleGreen)
                self.goBAck()

            } else {
                print("Response is not in json")
            }
        }
    }
}


