//
//  EditProfileVC.swift
//  StadiumUser
//
//  Created by XcelTec-053 on 30/03/21.
//  Copyright © 2021 XcelTec-053. All rights reserved.
//

import UIKit
import SVProgressHUD
import AVKit

class EditProfileVC: SuperViewController {

    @IBOutlet weak var txtPhoneNo: UITextField!
    @IBOutlet weak var txtFName: UITextField!
    @IBOutlet weak var txtLName: UITextField!

    @IBOutlet weak var txtEmail: UITextField!

    @IBOutlet weak var btnprofilepic: UIButton!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblContry: UILabel!
    var imagePicker = UIImagePickerController()
    var MyProfileData : ProfileModelData!

    var fname:String = ""
    var lname:String = ""
    var email:String = ""
    var profileImg:String = ""
    var strBase64 = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        imgProfile.layer.cornerRadius = imgProfile.bounds.size.width/2.0
        imgProfile.layer.masksToBounds = true
        imgProfile.sd_setImage(with: URL(string: profileImg), completed: nil)
        txtFName.text = fname
        txtLName.text = lname
        txtEmail.text = email
        
    }
    @IBAction func onClickBack(_ sender: UIButton) {
        Helper.sharedHelper.goToHome()
    }
    
  
    @IBAction func btnSaveProfileActions(_ sender: UIButton) {
        SVProgressHUD.show()
        print("Start:\(Date())")
        var imageData = imgProfile.image!.pngData()
        if((imgProfile.image?.description.contains("png")) != nil)
        {
            imageData = imgProfile.image!.pngData()
        }
        else
        {
            imageData = imgProfile.image!.jpegData(compressionQuality: 0.1)
        }
        
        strBase64 = imageData!.base64EncodedString(options: .init(rawValue: 0))
        
        APIEditProfile()
    }
    func APIEditProfile(){
       
        let parameter = ["first_name":txtFName.text!,"last_name":txtLName.text!,"image":strBase64] as [String : Any]
print("Start2:\(Date())")
        NetworkManager.shared.apiCallWithTokenNoLoading(Api: RUNNERPROFILE_URL, param: parameter, getView: self) { (response) in
            print("END:\(Date())")
                    
            SVProgressHUD.dismiss()
            if let getdata = response as? NSDictionary{
                print(getdata)
               
                if let data = getdata.value(forKey: DATA) as? String{
                     let DES = CryptoJS.AES()
                     let decryptedString = DES.decrypt(data, password: "rswna0hu8t")
                     print(decryptedString)
                     
                     let DecryptedData = decryptedString.data(using: .utf8)!
                     do{
                        let ModelData = try JSONDecoder().decode(RunnerData.self, from: DecryptedData)
                        //let fullName = ModelData.data!.first_name! + ModelData.data!.last_name!
                       // USERDEFAULT.set(fullName, forKey: KUser_name)
                        USERDEFAULT.set(ModelData.token!, forKey: kAccesstoken)
                       // USERDEFAULT.set(self.loginData.user_image, forKey: Kimage)
                        self.view.makeToast(getdata["message"] as? String ?? "", duration: TOAST_TIME, position: .bottom,style:ToaststyleGreen)
                        self.gotoHomee()
                     }catch{
                         debugPrint(error.localizedDescription)
                     }
                 }else{
                 print("NO data returned")
                 }
                 
                

                /*let model = ProfileModelClass.init(fromDictioanry: getdata)
                if model.code == 200{
                    
                    USERDEFAULT.set(model.data!.contact_number, forKey: KcontactNumber)
                    USERDEFAULT.set(model.data!.country_code, forKey: Kcountry_code)
                    USERDEFAULT.set(model.data!.country_code_name, forKey: Kcountry_code_name)
                    USERDEFAULT.set(model.data!.email, forKey: Kemail)
                    USERDEFAULT.set(model.data!.first_name, forKey: KFirst_name)
                    USERDEFAULT.set(model.data!.last_name, forKey: KLast_name)
                    USERDEFAULT.set(model.data!.user_image, forKey: Kimage)
                    
                    APP_DELEGATE.window?.makeToast(model.message, duration: TOAST_TIME, position: .bottom,style:ToaststyleGreen)
                        self.navigationController?.popViewController(animated: true)
                }else if model.code == 400 || model.code == 401 || model.code == 403  {
                    self.goToLogin()
                }else{
                    self.view.makeToast(model.message, duration: TOAST_TIME, position: .bottom,style:ToaststyleRed)
                }*/
            }else{
                self.view.makeToast(msgSomethingWentWrong, duration: TOAST_TIME, position: .bottom,style:ToaststyleRed)
            }
        }
    }
    func gotoHomee()
    {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            Helper.sharedHelper.goToHome()
        }
    }
    
    @IBAction func btnProfilePiciUploadAction(_ sender: UIButton) {
        let alert = UIAlertController.init(title: "", message: "Chose Profile Picture from?", preferredStyle: .alert)
        let camera = UIAlertAction.init(title: "Take Photo", style: .default, handler: {
            action in
            
            AVCaptureDevice.requestAccess(for: AVMediaType.video) { response in
                if response {
                    //access granted
                    DispatchQueue.main.async {
                        self.imagePicker.delegate = self
                        self.imagePicker.sourceType = .camera
                        self.imagePicker.allowsEditing = true
                        self.present(self.imagePicker, animated: true, completion: nil)
                    }
                } else {
                    print("Camera permission denide")
                }
            }
            
        })
        let gallery = UIAlertAction.init(title: "Choose from library", style: .default, handler: {
            action in
            self.imagePicker.delegate = self
            self.imagePicker.sourceType = .savedPhotosAlbum
            self.imagePicker.allowsEditing = true
            //self.modalPresentationStyle = .fullScreen
//            var navigationController = UINavigationController(rootViewController: self.imagePicker)
//            navigationController.modalPresentationStyle = .fullScreen
//            self.present(navigationController, animated: true, completion: nil)
            self.imagePicker.modalPresentationStyle = .fullScreen
            self.present(self.imagePicker, animated: true, completion: nil)
        })
        let cancel = UIAlertAction.init(title: "Cancel", style: .cancel, handler:nil)
        alert.addAction(camera)
        alert.addAction(gallery)
        alert.addAction(cancel)
        self.present(alert,animated: true)
    }
    
}
extension EditProfileVC:UINavigationControllerDelegate, UIImagePickerControllerDelegate{
    //MARK: - Imgae picker delegates
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let chosenImage = info[UIImagePickerController.InfoKey.editedImage] as! UIImage
        imgProfile.image = chosenImage
        
        var imageData = chosenImage.pngData()
        if((imgProfile.image?.description.contains("png")) != nil)
        {
            imageData = chosenImage.pngData()
        }
        else
        {
            imageData = chosenImage.jpegData(compressionQuality: 0.1)
        }
        
        strBase64 = imageData!.base64EncodedString(options: .init(rawValue: 0))
        picker.dismiss(animated: true, completion: nil);
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion:nil)
    }
}
struct RunnerProfileModel : Decodable {
    let message: String?
    let success: Bool?
    let success_code: Int?
    let data: RunnerData?
}

// MARK: - DataClass
struct RunnerData : Decodable {
    let user_id: Int?
    let token, first_name, last_name, mobile: String?
    let country_code, country_code_name: String?
    let email: String?
    let user_type, restaurant_id, stadium_id: Int?
}

