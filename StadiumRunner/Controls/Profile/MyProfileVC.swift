//
//  MyProfileVC.swift
//  StadiumUser
//
//  Created by XcelTec-053 on 30/03/21.
//  Copyright © 2021 XcelTec-053. All rights reserved.
//

import UIKit

class MyProfileVC: SuperViewController {

    @IBOutlet weak var lblEmailAccount: UILabel!
    @IBOutlet weak var lblContactNumber: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgprofile: UIImageView!
    var email:String = ""
    var fName:String = ""
    var lName:String = ""   
    
    var phoneNO = ""
    var profileURl = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        imgprofile.layer.cornerRadius = imgprofile.bounds.size.width/2.0
        imgprofile.layer.masksToBounds = true
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) { [self] in
            APIGetProfileData()
        }
      }
    
    @IBAction func btnEditProfileAction(_ sender: UIButton) {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)

        let vc = storyboard.instantiateViewController(withIdentifier: "EditProfileVC")as! EditProfileVC
        vc.profileImg = profileURl
        vc.email = email
        vc.fname = fName
        vc.lname = lName
        APP_DELEGATE.window?.rootViewController = vc
    }
    @IBAction func onClickBack(_ sender: UIButton) {
        Helper.sharedHelper.goToHome()
    }
    
    //MARK:- API call
    func APIGetProfileData()
    {
       let currentUserId = USERDEFAULT.value(forKey: Kuser_id) as? String
       if currentUserId! != nil {
        let apiURL = "\(GETVENDOR_URL)\(currentUserId!)"
        print("VenderApiURl:\(apiURL)")
        NetworkManager.shared.getAPICallWithToken(Api:apiURL, getView: self) { [self] (response) in
            if let getdata = response as? NSDictionary {
                print(getdata)
                if JSON(getdata.value(forKey: "success") as Any).boolValue{
                    if let data = getdata.value(forKey: DATA) as? String{
                        let DES = CryptoJS.AES()
                        let decryptedString = DES.decrypt(data, password: "rswna0hu8t")
                        print(decryptedString)
                        
                        let DecryptedData = decryptedString.data(using: .utf8)!
                        do{
                            let output = try JSONSerialization.jsonObject(with: DecryptedData, options: .allowFragments)
                            print(output)

                            let dic = JSON(output as Any).dictionaryValue

//                            print(dic["restaurant_name"]!.stringValue)
//                            self.lblLocation.text = String(describing: dic["restaurant_name"]!.stringValue)
//                            delivery_charge = dic["delivery_charge"]!.floatValue
                            
//                            service_charge = dic["service_charge"]!.floatValue
                            fName = dic["first_name"]!.stringValue
                            lName = dic["last_name"]!.stringValue
                            let fullName = "\(fName) \(lName)"
                            lblName.text = fullName
                               USERDEFAULT.setValue(fullName, forKey: Kfull_name)
                            email = "\(dic["vendor_email"]!.stringValue)"
                            lblEmail.text = email
                            lblEmailAccount.text = email
                            phoneNO = dic["vendor_contact_no"]!.stringValue
                            //lblContactNumber.text = phoneNO
                            let profileUrl = dic["profile"]!.stringValue
                            profileURl = profileUrl
                            imgprofile.sd_setImage(with: URL(string: profileUrl), completed: nil)
                            USERDEFAULT.set(profileUrl, forKey: Kimage)
                            /*
                             "city_id" = 1;
                             "country_id" = 1;
                             "first_name" = john;
                             id = 1;
                             "is_active" = Y;
                             "last_name" = doe;
                             profile = "";
                             "restaurant_id" = 3;
                             "stadium_id" = 13;
                             "state_id" = 1;
                             status = Busy;
                             username = "john doe";
                             "userrole_id" = 3;
                             "vendor_contact_no" = 6354915520;
                             "vendor_description" = "Fast Delivery";
                             "vendor_email" = "johndoe@mail.com";
                             */
                        }catch{
                            debugPrint(error.localizedDescription)
                        }
                    }else{
                        
                    }
                }else{
                    self.view.makeToast(JSON(getdata.value(forKey: "message") as Any).stringValue, duration: TOAST_TIME, position: .bottom,style:ToaststyleRed)
                }
            } else {
                print("Response is not in json")
            }

        }
    }
       /* NetworkManager.shared.apiCallWithToken(Api: CHANGEPASS_URL, param: parameter, getView: self) { (response) in
            if let getdata = response as? NSDictionary
            {
                print(getdata)
                let model = MainModelClass.init(data: getdata)
                if model.code == 200
                {
                    self.view.makeToast(model.message, duration: 2.0, position: .bottom,style:ToaststyleGreen)

                    self.goLogin()
                }else if model.code == 400 || model.code == 401 || model.code == 403   {
                    self.goToLogin()
                }else{
                    self.view.makeToast(model.message, duration: TOAST_TIME, position: .bottom,style:ToaststyleRed)
                }
            }else{
                self.view.makeToast(msgSomethingWentWrong, duration: TOAST_TIME, position: .bottom,style:ToaststyleRed)
            }
        }*/
    }
}

