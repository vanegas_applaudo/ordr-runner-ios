import Foundation
import UIKit

let TOAST_TIME = 3.0
let timeout = 10 //in seconds

let UDID = UIDevice.current.identifierForVendor!.uuidString

let USERDEFAULT = UserDefaults.standard
let APP_DELEGATE = UIApplication.shared.delegate as! AppDelegate
var ToaststyleRed = ToastStyle()
var ToaststyleGreen = ToastStyle()
let Main_STORY = UIStoryboard.init(name: "Main", bundle: nil)

let CODE = "code"
let MESSAGE = "message"
let DATA = "data"
let success = "200"
let isFirstTimeAd = "isFirsttime"
//MARK:- ----- key for parameters
//MARK:- ---- Login and signup
let kUsername = "username"
let kFcmToken = "fcm_token"
let kPassword = "password"
let kConfirmPassword = "confirm_password"
let kDeviceId = "user_device_id"
let Kuser_type = "user_type"
let kdevice_id = "device_id"
//WS response
let KcontactNumber = "contact_number"
let KmobileNumber = "mobile_number"
let Kcountry_code = "country_code"
var Kcountry_code_name = "country_code_name"
let Kcreated_date = "created_date"
let Kemail = "email"
let Kfcm_token = "fcm_token"
let Kfull_name = "full_name"
let Kstate_Id = "state_id"
let Kcountry_Id = "country_id"
let Kcity_Id = "city_id"
let Klogin_type = "login_type"
let Kstate = "state"
let Kcountry = "country"
let Kcity = "city"
let Kdevice_type = "device_type"

let Kimage = "image"
let Kis_notification = "is_notification"
//let kAccessToken = "token"
let Kuser_device_id = "user_device_id"
let Kuser_id = "user_id"
let Kotp = "otp"
let Kuser_image = "user_image"
let kAccesstoken = "access_token"
let KLatitude = "lat"
let KLongitude = "long"
let KRestaurantId = "restaurant_id"
let Kavailability_status = "availability_status"
//MARK:- ---- Vehicle

let kPage = "page"
let kPagination = "pagination"
let kNotificaiton = "notificaiton"
let kNotificationYes = "Y"
let kNotificationNo = "N"
let kNotification = "notification"
let kCurrentLatitude = "currentlatitude"
let kCurrentLongitude = "currentlongitude"
let kisFavourite = "is_favourite"
let kStatus = "status"
let kTitle = "title"
let kNewPassword = "newpassword"
let kOldPassword = "oldpassword"
let kProfilePhoto = "profile_photo"

// MARK: - Messages for SmartPark

//Login
let msgUserName = "Please enter username."
let msgPassword = "Please enter password."
let msgFullName = "Please enter your full name."
let msgCountry = "Please enter country."
let msgState = "Please enter state."
let msgCity = "Please enter city."
let msgEmail = "Please enter email."
let msgCorrectEmail = "Please enter valid email."
let msgMobileNumber = "Please enter phone number."
let msgConfirmPassword = "Please enter confirm password."
let msgSamePassword = "Password and confirm password does not match."
let msgcheckTerms = "Please select user agreement and privacy policy."
let msgProfilePicture = "Please upload your profile picture."
let msgEmptyOldPassword = "Please enter your old password."
let msgEmptyNewPassword = "Please enter your new password."
let msgEmptyConfirmPassword = "Please enter your confirm password."
let msgInternet = "Please check your internet connection and try again!!!"
let msgLogin = "You are not logged in yet."
let msgMinPass = "The password must be at least 8 characters."
let msgPasswordMin = "Password should contain 1 uppercase, 1 lowercase,  1 special character, 1 numeric character and should be minimum 8 character long."
let msgCreatePlaylist = "Please enter playlist name."
let msgOldNewSamePassword = "Old and New Password can not be same."
let msgUserNameSpace = "User Name should not include blank space."
let msgPasswordSpace = "Password should not include blank space."
let msgEmailSpace = "Email should not include blank space."
let msgLinkSent = "Password reset link has been sent to the mail."
let msgMinNewPassword = "The password must be atleast 6 characters."
let msgWebsite = "Please enter website."
let msgSomethingWentWrong = "Application is not working properly as per expected,please close it and again restart it"
let msgNoDataFound = "No data found."
let msgNoInternetConnection = "No internet connection found. Check your connection or try again."
let msgVehicleName = "Please enter Vehicle Name"
let msgVehicleNo = "Please enter Vehicle Number"
let msgVehicleType = "Please enter Vehicle Type"
let OldPasswordMsg = "Old Password is wrong."
let CurrentPasswordMsg = "Current Password is wrong."
let iosVersionError = "Available on ios 13 and above"
let msgPinDifferent = "Pin and Confirm Pin doen't match"
let msgPinWrong = "Please enter correct pin"
let ParkingAddedFAV = "Parking marked as favourite"
let ParkingRemovedFAV = "Parking removed from favourite"
let AddMoney = "Please enter amount to add money to wallet."
//MARK: - api Key name

let KID = "id"
let kImagePath = "imagePath"
//MARK:- ------ api urls

//local
let MAIN_URL = "https://ordrpartners.com:3005/stadium/"
//"https://stadium.siyadev.online:3005/stadium/"
//"https://ordrpartners.com:3005/stadium"
//let APP_LINK = "https://itunes.apple.com"

//MARK:-  -------- API endpoints

let LOGIN_URL = MAIN_URL + "login"
let NEWORDER_URL = MAIN_URL + "new_order_list"
let ACCEPTED_ORDER_URL = MAIN_URL + "accept_order_list"
let FORGOTPASS_URL = MAIN_URL + "forgetPassword"
let CHANGEPASS_URL = MAIN_URL + "runnerChangePassword"
let NOTIFYCOUNT_URL = MAIN_URL + "notifcationCount"
let GETNOTIFICATION = MAIN_URL + "getNotifications"
let READNOTIFICATION = MAIN_URL + "update_notification"

let REJECTORDER_URL = MAIN_URL + "reject_order"
let ORDERDETAILSRUNNER_URL = MAIN_URL + "order_details_runner"
let COMPLETEORDERLIST_URL = MAIN_URL + "completed_order_list"
let REJECTORDERLIST_URL = MAIN_URL + "rejected_order_list"
let ACCEPTORDERLIST_URL = MAIN_URL + "accept_order"
let RUNNERTOGGLE_URL = MAIN_URL + "mobile_self_runner_toggle"
let RUNNERPROFILE_URL = MAIN_URL + "runnerProfile"
let GETVENDOR_URL = MAIN_URL + "getVendorById/"
let RUNNERCOMPLETESTATUS_URL = MAIN_URL + "runner_complete_status"





























// All Api List https://docs.google.com/spreadsheets/d/1IC3D22hwT88FA35nPC2MOgK4_oNwAkLAUQFGGmpeCxA/edit#gid=964360698


//static used variablesunblock-member
var grid = "gridView"
var list = "listView"
var memberuserRole = "userRole"
let USER_DEVICEID = UIDevice.current.identifierForVendor?.uuidString


