import UIKit
import Alamofire
import SVProgressHUD

class Helper: NSObject {
    
    static let sharedHelper = Helper()
    
    
    func isNetworkAvailable() -> Bool
    {
        let rechability = Reachability()
        
        if (rechability?.isReachable)!
        {
            return true
        }
        else
        {
            return false
        }
    }
    
    func setCornerRadiusButton(radius : CGFloat, btn: UIButton){
        btn.layer.cornerRadius = radius
        btn.layer.masksToBounds = true
    }
    
    func hideFooterSpace(tableView : UITableView){
        tableView.tableFooterView = UIView.init(frame: .zero)
    }
    
    func addPadding(txtFiled: UITextField) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 35, height: 0))
        txtFiled.leftViewMode = .always
        txtFiled.leftView = paddingView
    }

    func validateEmailWithString(_ checkString : NSString) -> Bool
    {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: checkString)
        
    }
    
    func passwordValidation(_ checkPass : NSString) -> Bool
    {
        let passwordTest = NSPredicate(format: "SELF MATCHES %@", "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@!?<>%*()/-_#$%^&+=])(?=\\S+$).{8,}$")
        return passwordTest.evaluate(with: checkPass)
    }
   
    func isUserLoggedIn() -> Bool
    {
        if USERDEFAULT.object(forKey: kAccesstoken) != nil
        {
            return true
        }
        return false
    }
    
//    //  MARK:  NSUserDefault Handlers
//    func setDefaultObject(dict : [String : AnyObject], key:String)
//    {
//        let encodedData = NSKeyedArchiver.archivedData(withRootObject: dict)
//        let userDefaults = UserDefaults.standard
//        userDefaults.set(encodedData, forKey: key)
//    }
    
//    func getDefaultObjectForKey(key : String) -> [String : AnyObject]
//    {
//        let decoded  = UserDefaults.standard.object(forKey: key) as! Data
//        let decodedTeams = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! [String : AnyObject]
//        return decodedTeams
//    }
//    
    func setBoolToUserdefault(value: Bool, forKey defaultName: String){
        let defaults = UserDefaults.standard
        defaults.set(value, forKey: defaultName)
        defaults.synchronize()
    }
    
    func generateUniqueStr() -> String
    {
        let date = Date()
        let formater = DateFormatter()
        formater.dateFormat = "dd:MM:yyyy:mm:ss:SSS"
        let DateStr = formater.string(from: date) as String
        return DateStr.replacingOccurrences(of: ":", with: "")
    }
    
    func logout(comp : @escaping () -> ()) {
//        if #available(iOS 13.0, *) {
//            let _appDelegate = UIApplication.shared.delegate as! AppDelegate
//        } else {
            let _appDelegate = UIApplication.shared.delegate as! AppDelegate
//        }
        if let nav = _appDelegate.window?.rootViewController as? UINavigationController {
            nav.dismiss(animated: false, completion: nil)
            _ = nav.popViewController(animated: false)
            comp()
        }
    }
    
    func convertDate(date : String, dateFormat:DateFormatter) -> String
    {
        dateFormat.dateFormat = "yyyy-MM-dd hh:mm:ss"
        let date = dateFormat.date(from: date)
        dateFormat.dateFormat = "dd-MM-yyyy"
        return date == nil ? "-" : String(format : "%@",dateFormat.string(from: date!))
    }
    
    func convertDate(date : String) -> String
    {
        let strdate = date
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let convdate = dateFormat.date(from: strdate)
        dateFormat.dateFormat = "hh:mm a dd-MMM-yyyy"
        return convdate == nil ? "-" : String(format : "%@",dateFormat.string(from: convdate!))
    }
    
    func prepareImgUrl(url : String) -> String
    {
        let Str = String(format: url)
        let finalStr = Str.replacingOccurrences(of: " ", with: "%20")
        return finalStr
    }
    
    func goToLogin()
    {
        USERDEFAULT.removeObject(forKey: "LocationEnable")
        USERDEFAULT.removeObject(forKey: Kuser_id)
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "LoginVC")as! LoginVC
//        let navigat = UINavigationController()
        APP_DELEGATE.window?.rootViewController = vc
    }

    func forgotPwd()
    {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ForgotPasswordVC")as! ForgotPasswordVC
//        let navigat = UINavigationController()
        APP_DELEGATE.window?.rootViewController = vc
    }
    func goToNotification()
    {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "NotificationVC") as! NotificationVC
        APP_DELEGATE.window?.rootViewController = vc
    }
    func gotoMyOrder()
    {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "MyOrderListVC")as! MyOrderListVC
//        let navigat = UINavigationController()
        APP_DELEGATE.window?.rootViewController = vc
    }
    
    
    
    func goToHome() // side bar work karse
    {
        let navigation = Main_STORY.instantiateViewController(withIdentifier: "dashNav") as! UINavigationController
        navigation.isNavigationBarHidden = true

        let mainViewController = Main_STORY.instantiateViewController(withIdentifier: "DashboardVC") as! DashboardVC
        let leftViewController = Main_STORY.instantiateViewController(withIdentifier: "LeftMenuVC") as! LeftMenuVC

        let slideMenuController = SlideMenuController(mainViewController:navigation, leftMenuViewController: leftViewController, rightMenuViewController: leftViewController)
        slideMenuController.delegate = mainViewController as? SlideMenuControllerDelegate
        APP_DELEGATE.window?.rootViewController = slideMenuController
    }
    
    func goToDashboard(){ // sidebar work nai kare
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let navigation = storyboard.instantiateViewController(withIdentifier: "dashNav") as! UINavigationController
        navigation.isNavigationBarHidden = true
        APP_DELEGATE.window?.rootViewController = navigation
    }
}

//"(http|https)://((\\w)*|([0-9]*)|([-|_])*)+([\\.|/]((\\w)*|([0-9]*)|([-|_])*))+"
extension String {
func validateUrl () -> Bool {
        //let urlRegEx = "((?:http|https)://)?(?:www\\.)?[\\w\\d\\-_]+\\.\\w{2,3}(\\.\\w{2})?(/(?<=/)(?:[\\w\\d\\-./_]+)?)?"
    let urlRegEx = "(http|https)://((\\w)*|([0-9]*)|([-|_])*)+([\\.|/]((\\w)*|([0-9]*)|([-|_])*))+"
        return NSPredicate(format: "SELF MATCHES %@", urlRegEx).evaluate(with: self)
    }
}


extension UITableView {
    
    func setEmptyView(title: String, message: String, messageImage: UIImage) {
        
        let emptyView = UIView(frame: CGRect(x: self.center.x, y: self.center.y, width: self.bounds.size.width, height: self.bounds.size.height))
        
        let messageImageView = UIImageView()
        let titleLabel = UILabel()
        let messageLabel = UILabel()
        
        messageImageView.backgroundColor = .clear
        
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        messageImageView.translatesAutoresizingMaskIntoConstraints = false
        messageLabel.translatesAutoresizingMaskIntoConstraints = false
        
        titleLabel.textColor = #colorLiteral(red: 0, green: 0.2, blue: 0.3215686275, alpha: 1)
        titleLabel.font = UIFont(name: "HelveticaNeue-Bold", size: 18)
        
        messageLabel.textColor = #colorLiteral(red: 0, green: 0.2, blue: 0.3215686275, alpha: 1)
        messageLabel.font = UIFont(name: "HelveticaNeue-Bold", size: 18)
        
      //  emptyView.addSubview(titleLabel)
     //   emptyView.addSubview(messageImageView)
        emptyView.addSubview(messageLabel)
        
//        messageImageView.centerXAnchor.constraint(equalTo: emptyView.centerXAnchor).isActive = true
//        messageImageView.centerYAnchor.constraint(equalTo: emptyView.centerYAnchor, constant: -50).isActive = true
//        messageImageView.widthAnchor.constraint(equalToConstant: 220).isActive = true
//        messageImageView.heightAnchor.constraint(equalToConstant: 200).isActive = true
//        
//        titleLabel.topAnchor.constraint(equalTo: messageImageView.bottomAnchor, constant: 20).isActive = true
//        titleLabel.centerXAnchor.constraint(equalTo: emptyView.centerXAnchor).isActive = true
        
       // messageLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 10).isActive = true
        messageLabel.centerYAnchor.constraint(equalTo: emptyView.centerYAnchor, constant: 0).isActive = true
        messageLabel.centerXAnchor.constraint(equalTo: emptyView.centerXAnchor).isActive = true
        
        messageImageView.image = messageImage
        titleLabel.text = title
        messageLabel.text = message
        messageLabel.numberOfLines = 0
        messageLabel.textAlignment = .center

        self.backgroundView = emptyView
        self.separatorStyle = .none
    }
    
    func restore() {
        self.backgroundView = nil
        self.separatorStyle = .none
    }
    
}

extension UICollectionView {
    func setEmptyView(title: String, message: String, messageImage: UIImage) {
        
        let emptyView = UIView(frame: CGRect(x: self.center.x, y: self.center.y, width: self.bounds.size.width, height: self.bounds.size.height))
        
        let messageImageView = UIImageView()
        let titleLabel = UILabel()
        let messageLabel = UILabel()
        
        messageImageView.backgroundColor = .clear
        
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        messageImageView.translatesAutoresizingMaskIntoConstraints = false
        messageLabel.translatesAutoresizingMaskIntoConstraints = false
        
        titleLabel.textColor = #colorLiteral(red: 0, green: 0.2, blue: 0.3215686275, alpha: 1)
        titleLabel.font = UIFont(name: "HelveticaNeue-Bold", size: 18)
        
        messageLabel.textColor = #colorLiteral(red: 0, green: 0.2, blue: 0.3215686275, alpha: 1)
        messageLabel.font = UIFont(name: "HelveticaNeue-Bold", size: 18)
        
        //emptyView.addSubview(titleLabel)
       // emptyView.addSubview(messageImageView)
        emptyView.addSubview(messageLabel)
        
//        messageImageView.centerXAnchor.constraint(equalTo: emptyView.centerXAnchor).isActive = true
//        messageImageView.centerYAnchor.constraint(equalTo: emptyView.centerYAnchor, constant: -50).isActive = true
//        messageImageView.widthAnchor.constraint(equalToConstant: 220).isActive = true
//        messageImageView.heightAnchor.constraint(equalToConstant: 200).isActive = true
//        
//        titleLabel.topAnchor.constraint(equalTo: messageImageView.bottomAnchor, constant: 20).isActive = true
//        titleLabel.centerXAnchor.constraint(equalTo: emptyView.centerXAnchor).isActive = true
//        
        messageLabel.centerYAnchor.constraint(equalTo: emptyView.centerYAnchor, constant: 0).isActive = true
        messageLabel.centerXAnchor.constraint(equalTo: emptyView.centerXAnchor).isActive = true
        
        messageImageView.image = messageImage
        titleLabel.text = title
        messageLabel.text = message
        messageLabel.numberOfLines = 0
        messageLabel.textAlignment = .center
        
        self.backgroundView = emptyView
        self.indicatorStyle = .default
    }
    
    func restore() {
        
        self.backgroundView = nil
        
    }
}
