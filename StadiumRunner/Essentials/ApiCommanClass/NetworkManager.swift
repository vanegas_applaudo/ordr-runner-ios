//
//  ParentController.swift
//  Expect
//
//  Created by XcelTec-053 on 14/02/19.
//  Copyright © 2019 XcelTec-054. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD
import SDWebImage

class NetworkManager {
    static let shared = NetworkManager()
}

extension NetworkManager{
    /**
     post api call
     */
    func jsonConvert(from object:Any) -> String? {
        guard let data = try? JSONSerialization.data(withJSONObject: object, options: []) else {
            return nil
        }
        return String(data: data, encoding: String.Encoding.utf8)
    }
    
//    func apiCall(Api : String , param : Parameters,getView : UIViewController , comp : @escaping (Any)->()){
//        ToaststyleRed.backgroundColor = #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1)
//        ToaststyleGreen.backgroundColor = #colorLiteral(red: 0, green: 0.5882352941, blue: 0.2509803922, alpha: 1)
//        SVProgressHUD.show()
//         print("EndPint>>>>>\(Api)")
//
//        //print(jsonConvert(from: param)!)
//        let key = "rswna0hu8t"
//
//        let AES = CryptoJS.AES()
//        let cipherText = AES.encrypt(self.jsonConvert(from: param)!, password: key)
//        print("encryptPlain:>>>>>\(cipherText as String)")
//
//        let jsonData = cipherText.data(using: .utf8, allowLossyConversion: false)!
//        AF.session.configuration.timeoutIntervalForRequest = TimeInterval(20)
//
//        let url = URL(string: Api)!
//        var request = URLRequest(url: url)
//        request.httpMethod = HTTPMethod.post.rawValue
//        request.setValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
//        request.httpBody = jsonData
//
//        //AF.request(request).responseString { (response) in
//
//        AF.request(request).responseData(completionHandler: { (response) in
//            print("response>>>>>>>",String(data: response.data!, encoding: String.Encoding.utf8)!)
//            SVProgressHUD.dismiss()
//            switch response.result
//            {
//            case .success(_):
//
//                if(response.data != nil)
//                {
//                    let value = String(data: response.data!, encoding: String.Encoding.utf8)!
//                    if(value.contains("doctype")  || value.contains("DOCTYPE"))
//                    {
//                        UIApplication.getTopViewController()?.view.makeToast(msgSomethingWentWrong, duration: TOAST_TIME, position: .bottom,style:ToaststyleRed)
//                    }
//                    else
//                    {
//                        let decryptedString = cryptLib.decryptCipherTextRandomIV(withCipherText: (value), key: key)
//
//                        if(decryptedString?.count == 0)
//                        {
//                            UIApplication.getTopViewController()?.view.makeToast(msgSomethingWentWrong, duration: TOAST_TIME, position: .bottom,style:ToaststyleRed)
//                        }
//                        else
//                        {
//                            let data = decryptedString?.data(using: .utf8)!
//                            do{
//                                let output = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [String:Any]
//                                comp(output! as NSDictionary)
//                            }
//                            catch {
//                                UIApplication.getTopViewController()?.view.makeToast(error.localizedDescription, duration: TOAST_TIME, position: .bottom,style:ToaststyleRed)
//                            }
//                        }
//                    }
//
//                }
//                else
//                {
//                    UIApplication.getTopViewController()?.view.makeToast(msgSomethingWentWrong, duration: TOAST_TIME, position: .bottom,style:ToaststyleRed)
//                }
//
//            case .failure(_):
//                comp("")
//                if Helper.sharedHelper.isNetworkAvailable() == true {
//                    UIApplication.getTopViewController()?.view.makeToast(msgSomethingWentWrong, duration: TOAST_TIME, position: .bottom,style:ToaststyleRed)
//                }else{
//                    UIApplication.getTopViewController()?.view.makeToast(msgInternet, duration: TOAST_TIME, position: .bottom,style:ToaststyleRed)
//                }
//            }
//        })
//    }
    
    
    func apiCall(Api : String , param : Parameters,getView : UIViewController , comp : @escaping (Any)->()){
        ToaststyleRed.backgroundColor = #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1)
        ToaststyleGreen.backgroundColor = #colorLiteral(red: 0, green: 0.5882352941, blue: 0.2509803922, alpha: 1)
        SVProgressHUD.show()
        guard let getApi = URL(string: Api) else {
            return
        }

        let AES = CryptoJS.AES()
        let encrypted = AES.encrypt(jsonConvert(from: param)!, password: "rswna0hu8t")
        print("encryptedPlain:>>>>>",encrypted)
        
        let parameter = ["info":encrypted] as [String : Any]
        
        AF.session.configuration.timeoutIntervalForRequest = TimeInterval(20)

        AF.request(getApi, method: .post, parameters: parameter , encoding: URLEncoding.default, headers: nil)
            .responseJSON { response in
                SVProgressHUD.dismiss()
                switch response.result {
                case .success(let value):
                    print("\n\(getApi)\n")
                    print("\n\(param)\n")
                    print("\n\(value)\n")
                    comp(value)
                    break
                case .failure(let error):
                    comp("")
                    if Helper.sharedHelper.isNetworkAvailable() == true {
                        UIApplication.getTopViewController()?.view.makeToast(msgSomethingWentWrong, duration: TOAST_TIME, position: .bottom,style:ToaststyleRed)
                    }else{
                        UIApplication.getTopViewController()?.view.makeToast(msgInternet, duration: TOAST_TIME, position: .bottom,style:ToaststyleRed)
                    }
                    print(error.localizedDescription)
                    break
                }
        }
    }
    func apiCallWithTokenNoLoading(Api : String , param : Parameters,getView : UIViewController , comp : @escaping (Any)->()){
        ToaststyleRed.backgroundColor = #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1)
        ToaststyleGreen.backgroundColor = #colorLiteral(red: 0, green: 0.5882352941, blue: 0.2509803922, alpha: 1)

        guard let getApi = URL(string: Api) else {
            return
        }

        let AES = CryptoJS.AES()
        let encrypted = AES.encrypt(jsonConvert(from: param)!, password: "rswna0hu8t")
        print("encryptedPlain:>>>>>",encrypted)
        
        var Token = ""
        if let token = USERDEFAULT.value(forKey: kAccesstoken) as? String{
            Token = token
        }
        
        let headers: HTTPHeaders = [.authorization(bearerToken: Token)]
        
        let parameter = ["info":encrypted] as [String : Any]
        
        AF.session.configuration.timeoutIntervalForRequest = TimeInterval(20)

        AF.request(getApi, method: .post, parameters: parameter , encoding: URLEncoding.default, headers: headers)
            .responseJSON { response in
                switch response.result {
                case .success(let value):
                    print("\n\(getApi)\n")
                    print("\n\(param)\n")
                    print("\n\(value)\n")
                    comp(value)
                    break
                case .failure(let error):
                    comp("")
                    if Helper.sharedHelper.isNetworkAvailable() == true {
                        UIApplication.getTopViewController()?.view.makeToast(msgSomethingWentWrong, duration: TOAST_TIME, position: .bottom,style:ToaststyleRed)
                    }else{
                        UIApplication.getTopViewController()?.view.makeToast(msgInternet, duration: TOAST_TIME, position: .bottom,style:ToaststyleRed)
                    }
                    print(error.localizedDescription)
                    break
                }
         }
    }
    
    func apiCallWithToken(Api : String , param : Parameters,getView : UIViewController , comp : @escaping (Any)->()){
         SVProgressHUD.show()
        ToaststyleRed.backgroundColor = #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1)
        ToaststyleGreen.backgroundColor = #colorLiteral(red: 0, green: 0.5882352941, blue: 0.2509803922, alpha: 1)

        guard let getApi = URL(string: Api) else {
            return
        }

        let AES = CryptoJS.AES()
        let encrypted = AES.encrypt(jsonConvert(from: param)!, password: "rswna0hu8t")
        print("encryptedPlain:>>>>>",encrypted)
        
        var Token = ""
        if let token = USERDEFAULT.value(forKey: kAccesstoken) as? String{
            Token = token
        }
        
        let headers: HTTPHeaders = [.authorization(bearerToken: Token)]
        
        let parameter = ["info":encrypted] as [String : Any]
        
        AF.session.configuration.timeoutIntervalForRequest = TimeInterval(20)

        AF.request(getApi, method: .post, parameters: parameter , encoding: URLEncoding.default, headers: headers)
            .responseJSON { response in
                SVProgressHUD.dismiss()
                switch response.result {
                case .success(let value):
                    print("\n\(getApi)\n")
                    print("\n\(param)\n")
                    print("\n\(value)\n")
                    comp(value)
                    break
                case .failure(let error):
                    comp("")
                    if Helper.sharedHelper.isNetworkAvailable() == true {
                        UIApplication.getTopViewController()?.view.makeToast(msgSomethingWentWrong, duration: TOAST_TIME, position: .bottom,style:ToaststyleRed)
                    }else{
                        UIApplication.getTopViewController()?.view.makeToast(msgInternet, duration: TOAST_TIME, position: .bottom,style:ToaststyleRed)
                    }
                    print(error.localizedDescription)
                    break
                }
         }
    }
    
    
    func apiDeleteCall(Api : String , param : Parameters,getView : UIViewController , comp : @escaping (Any)->()){
        SVProgressHUD.show()
        ToaststyleRed.backgroundColor = #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1)
        ToaststyleGreen.backgroundColor = #colorLiteral(red: 0, green: 0.5882352941, blue: 0.2509803922, alpha: 1)
        
        guard let getApi = URL(string: Api) else {
            return
        }
        
        AF.session.configuration.timeoutIntervalForRequest = TimeInterval(20)

        AF.request(getApi, method: .delete, parameters: param , encoding: URLEncoding.default, headers: nil)
            .responseJSON { response in
                SVProgressHUD.dismiss()
                switch response.result {
                case .success(let value):
                    print("\n\(getApi)\n")
                    print("\n\(param)\n")
                    print("\n\(value)\n")
                    comp(value)
                    break
                case .failure(let error):
                    // error handling
                    comp("")
                    if Helper.sharedHelper.isNetworkAvailable() == true {
                      APP_DELEGATE.window?.makeToast(msgSomethingWentWrong, duration: TOAST_TIME, position: .bottom,style:ToaststyleRed)
                    }else{
                        APP_DELEGATE.window?.makeToast(msgInternet, duration: TOAST_TIME, position: .bottom,style:ToaststyleRed)
                    }
                    print(error.localizedDescription)
                    break
                }
        }
    }
    /**
     get api call
     */
    func getApiCall(Api : String ,getView : UIViewController , comp : @escaping (Any)->()){
        ToaststyleRed.backgroundColor = #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1)
        ToaststyleGreen.backgroundColor = #colorLiteral(red: 0, green: 0.5882352941, blue: 0.2509803922, alpha: 1)
        
        guard let getApi = URL(string: Api) else {
            return
        }
         AF.session.configuration.timeoutIntervalForRequest = TimeInterval(20)

        AF.request(getApi).responseJSON { (response) in
            SVProgressHUD.dismiss()
            switch (response.result) {
            case .success:
                print("******************************************")
                print("\n\(getApi)\n")
                print("\n\(response)\n")
                print("******************************************")
                comp(response)
                break
            case .failure(let error):
                comp("")
                print(error.localizedDescription)
                APP_DELEGATE.window?.makeToast(msgSomethingWentWrong, duration: TOAST_TIME, position: .bottom,style:ToaststyleRed)
                break
            }
        }
    }
    
    
    func getAPICallWithToken(Api : String,getView : UIViewController , comp : @escaping (Any)->()){
//         SVProgressHUD.show()
        ToaststyleRed.backgroundColor = #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1)
        ToaststyleGreen.backgroundColor = #colorLiteral(red: 0, green: 0.5882352941, blue: 0.2509803922, alpha: 1)

        guard let getApi = URL(string: Api) else {
            return
        }

        
        var Token = ""
        if let token = USERDEFAULT.value(forKey: kAccesstoken) as? String{
            Token = token
            print("TOKEN======",token,"==========")
        }
        
        let headers: HTTPHeaders = [.authorization(bearerToken: Token)]
        
        
        AF.session.configuration.timeoutIntervalForRequest = TimeInterval(20)

        AF.request(getApi, method: .get, parameters: nil , encoding: URLEncoding.default, headers: headers)
            .responseJSON { response in
                SVProgressHUD.dismiss()
                switch response.result {
                case .success(let value):
                    print("\n\(getApi)\n")
                    print("\n\(value)\n")
                    
                    if response.response?.statusCode == 401 || response.response?.statusCode == 403{
                        debugPrint("force logout")
                        
                        Helper.init().goToLogin()
                        return
                    }
                    
                    comp(value)
                    break
                case .failure(let error):
                    if let responseData = response.data {
                        let htmlString = String(data: responseData, encoding: .utf8)
                        debugPrint(htmlString!)
                    }
                    comp("Reponse Error")
                    if Helper.sharedHelper.isNetworkAvailable() == true {
                        UIApplication.getTopViewController()?.view.makeToast(msgSomethingWentWrong, duration: TOAST_TIME, position: .bottom,style:ToaststyleRed)
                    }else{
                        UIApplication.getTopViewController()?.view.makeToast(msgInternet, duration: TOAST_TIME, position: .bottom,style:ToaststyleRed)
                    }
                    print(error.localizedDescription)
                    break
                }
         }
    }
    /**
     upload image api call
     //     */
    //    func uploadImage(getUrl : String,ParamDict : [String : Any],image : [UIImage],getfileName : String,imgKeyName : String, comp: @escaping (Any) -> ())
    //    {
    //        ToaststyleRed.backgroundColor = #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1)
    //        ToaststyleGreen.backgroundColor = #colorLiteral(red: 0, green: 0.5882352941, blue: 0.2509803922, alpha: 1)
    //
    //        let URL = try! URLRequest(url: getUrl, method: .post, headers: nil)
    //        SVProgressHUD.show(withStatus: "Please wait")
    //        AF.session.configuration.timeoutIntervalForRequest = TimeInterval(20)
    //
    //        AF.upload(multipartFormData: { (multipartFormData) in
    //            for images in image {
    ////                if (images.imageIsNullOrNot()) {
    ////
    ////                    let imgData = images.jpegData(compressionQuality: 0.5)!
    ////                    multipartFormData.append(imgData, withName: imgKeyName, fileName: getfileName, mimeType: "jpeg/jpeg")
    ////                }
    //            }
    //            for (key, value) in ParamDict {
    //                multipartFormData.append(((value) as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key )
    //            }
    //
    //        }, with: URL, encodingCompletion: { (result) in
    //
    //            switch result {
    //            case .success(let upload, _, _):
    //
    //                upload.uploadProgress { progress in
    ////                    let progres = String(format: "%0.2f", progress.fractionCompleted)
    ////                    let convertedString = Double(progres)! * 100
    ////
    ////                    APP_DELEGATE.window?.makeToast("\(Int(convertedString))% Completed", duration: TOAST_TIME, position: .bottom)
    //                }
    //                upload.responseJSON { response in
    //                    comp(response.result.value!)
    //                }
    //            case .failure(let encodingError):
    //                SVProgressHUD.dismiss()
    //                comp("")
    //                APP_DELEGATE.window?.makeToast(encodingError.localizedDescription, duration: TOAST_TIME, position: .bottom,style:ToaststyleRed)
    //                break
    //            }
    //        })
    //    }
}

