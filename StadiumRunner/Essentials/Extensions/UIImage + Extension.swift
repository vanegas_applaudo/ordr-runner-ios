//
//  UIImageView.swift
//  
//
//  Created by XcelTec-053 on 06/05/19.
//  Copyright © 2019 xceltec. All rights reserved.
//

import Foundation
import UIKit

extension UIImage {
    /**
     returns false when image is null and true when image is not null
     */
    func imageIsNullOrNot()-> Bool
    {
        let size = CGSize(width: 0, height: 0)
        if (self.size == size)
        {
            return false
        }
        else
        {
            return true
        }
    }
}

extension Date {
    func toMillis() -> Int64! {
        return Int64(self.timeIntervalSince1970 * 1000)
    }
}
