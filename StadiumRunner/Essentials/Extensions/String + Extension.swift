//
//  String.swift
//  data
//
//  Created by XcelTec-053 on 06/05/19.
//  Copyright © 2019 xceltec. All rights reserved.
//

import Foundation
import UIKit

extension String {
    /**
     it will generate random number
    */
    static func random(length: Int = 20) -> String {
        let base = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        var randomString: String = ""
        
        for _ in 0..<length {
            let randomValue = arc4random_uniform(UInt32(base.count))
            randomString += "\(base[base.index(base.startIndex, offsetBy: Int(randomValue))])"
        }
        return randomString
    }
    
    /**
     it will generate Attributed string with asked parameters
    */
    func attributedText(fontName : UIFont,fontSize : CGFloat,attributedColor : UIColor,startingIndex : Int,totalLength : Int) -> NSAttributedString{
        let strVal = NSMutableAttributedString(string: self, attributes: [NSAttributedString.Key.font:fontName])
        strVal.addAttribute(NSAttributedString.Key.foregroundColor, value: attributedColor, range: NSRange(location:startingIndex,length:totalLength))
        return strVal
    }
    /**
     it will generate Attributed string and append two attributed String.
     */
    func appendingAttributedString(attributedString1 : NSAttributedString ,attributedString2 : NSAttributedString,attributedString3 : NSAttributedString ) -> NSMutableAttributedString{
        let attribute = NSMutableAttributedString()
        attribute.append(attributedString1)
        attribute.append(attributedString2)
        attribute.append(attributedString3)
        return attribute
    }

    /**
     it will generate UIColor from given hex color string with default alpha of 1
    */
//    func hexToColor(alpha:CGFloat? = 1.0) -> UIColor {
//        var hexInt: UInt32 = 0
//        // Create scanner
//        let scanner: Scanner = Scanner(string: self)
//        // Tell scanner to skip the # character
//        scanner.charactersToBeSkipped = CharacterSet(charactersIn: "#")
//        // Scan hex value
//        scanner.scanHexInt32(<#T##result: UnsafeMutablePointer<UInt32>?##UnsafeMutablePointer<UInt32>?#>)
//        //64(&hexInt)
//        //scanHexInt32(&hexInt)
//
//        let hexint = Int(hexInt)
//        let red = CGFloat((hexint & 0xff0000) >> 16) / 255.0
//        let green = CGFloat((hexint & 0xff00) >> 8) / 255.0
//        let blue = CGFloat((hexint & 0xff) >> 0) / 255.0
//        let alpha = alpha!
//
//        // Create color object, specifying alpha as well
//        let color = UIColor(red: red, green: green, blue: blue, alpha: alpha)
//        return color
//    }
    
    /**
      Convert html from text string
    */
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
    
    /**
      Convert Html to Attributed String
    */
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }
    /**
     Defines safetly limit
     */
    func safelyLimitedTo(length n: Int)->String {
        if (self.count <= n) {
            return self
        }
        return String( Array(self).prefix(upTo: n) )
    }
    
    var encodeWithEmoji : String {
        let data = self.data(using: String.Encoding.nonLossyASCII, allowLossyConversion: true)
        if let data = data {
            let emojiString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)! as String
            return emojiString
        }
        return self
    }
    
    var decodeEmoji: String {
        let data = self.data(using: String.Encoding.utf8);
        let decodedStr = NSString(data: data!, encoding: String.Encoding.nonLossyASCII.rawValue)
        if let str = decodedStr{
            return str as String
        }
        return self
    }    
}
extension String {
    func SizeOf_String( font: UIFont) -> CGFloat {
        let fontAttribute = [NSAttributedString.Key.font: font]
        let size = self.size(withAttributes: fontAttribute)
        return size.width
    }
}
