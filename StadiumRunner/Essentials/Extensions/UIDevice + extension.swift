//
//  UIDevice + extension.swift
//  iJustPaid
//
//  Created by XcelTec-053 on 22/06/19.
//  Copyright © 2019 xceltec. All rights reserved.
//

import Foundation
import UIKit

extension UIDevice {
    
    public class var isPhone: Bool {
        return UIDevice.current.userInterfaceIdiom == .phone
    }
    
    public class var isPad: Bool {
        return UIDevice.current.userInterfaceIdiom == .pad
    }
    
    public class var isTV: Bool {
        return UIDevice.current.userInterfaceIdiom == .tv
    }
    
    public class var isCarPlay: Bool {
        return UIDevice.current.userInterfaceIdiom == .carPlay
    }
    
}
