//
//  Date + Extension.swift
//  iJustPaid
//
//  Created by XcelTec-053 on 19/06/19.
//  Copyright © 2019 xceltec. All rights reserved.
//

import Foundation

extension Date {
    
    func offsetFrom(date : Date) -> String {
        
        let dayHourMinuteSecond: Set<Calendar.Component> = [.day, .hour, .minute, .second]
        let difference = NSCalendar.current.dateComponents(dayHourMinuteSecond, from: date, to: self)
        
        let seconds = "\(difference.second ?? 0) sec ago"
        let minutes = "\(difference.minute ?? 0) mins ago"// + " " + seconds
        let hours = "\(difference.hour ?? 0) hrs ago"// + " " + minutes
        var days = ""
        if difference.day! == 1 {
            days = "1 day ago"
        }
        
        
        if let dateYes = difference.day, dateYes > 1 {
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let myString = formatter.string(from: Date())
            let yourDate = formatter.date(from: myString)
            formatter.dateFormat = "dd/MM/yyyy"
            return formatter.string(from: yourDate!)
        }
        
        if let day = difference.day, day          > 0 { return days }
        if let hour = difference.hour, hour       > 0 { return hours }
        if let minute = difference.minute, minute > 0 { return minutes }
        if let second = difference.second, second > 0 { return seconds }
        return ""
    }
    

}
extension String {
    
//    func getFormatDate(format: String) -> String {
//         let dateformat = DateFormatter()
//         dateformat.dateFormat = format
//         return dateformat.string(from: String)
//     }

    static func getFormattedDate(string: String , formatter:String) -> String{
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"

        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "HH:mm a"

        let date: Date? = dateFormatterGet.date(from: "2018-02-01T19:10:04+00:00")
        print("Date",dateFormatterPrint.string(from: date!)) // Feb 01,2018
        return dateFormatterPrint.string(from: date!);
    }

    
}

