//
//  UIAlertControllerExtension.swift
//  
//
//  Created by XcelTec-047 on 18/09/18.
//  Copyright © 2018 XcelTec-Mac Air. All rights reserved.
//

import Foundation
import UIKit

extension UIAlertController {
    
    class func actionWith(title: String?, message: String?, style: UIAlertController.Style, buttons: [String], controller: UIViewController?, userAction: ((_ action: String) -> ())?) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: style)
        buttons.forEach { (buttonTitle) in
            
            if buttonTitle == "Logout" {
                alertController.addAction(UIAlertAction(title: buttonTitle, style: .destructive, handler: { (action: UIAlertAction) in
                    userAction?(buttonTitle)
                }))
            }else if buttonTitle == "Report Images" {
                alertController.addAction(UIAlertAction(title: buttonTitle, style: .destructive, handler: { (action: UIAlertAction) in
                    userAction?(buttonTitle)
                }))
            }else if buttonTitle == "Report User" {
                alertController.addAction(UIAlertAction(title: buttonTitle, style: .destructive, handler: { (action: UIAlertAction) in
                    userAction?(buttonTitle)
                }))
            }else if buttonTitle == "Sign Out" {
                alertController.addAction(UIAlertAction(title: buttonTitle, style: .destructive, handler: { (action: UIAlertAction) in
                    userAction?(buttonTitle)
                }))
            }else if buttonTitle == "Yes" {
                alertController.addAction(UIAlertAction(title: buttonTitle, style: .default, handler: { (action: UIAlertAction) in
                    userAction?(buttonTitle)
                }))
            }else if buttonTitle == "Block User" {
                alertController.addAction(UIAlertAction(title: buttonTitle, style: .destructive, handler: { (action: UIAlertAction) in
                    userAction?(buttonTitle)
                }))
            }else if buttonTitle == "Cancel MeetUp" {
                alertController.addAction(UIAlertAction(title: buttonTitle, style: .destructive, handler: { (action: UIAlertAction) in
                    userAction?(buttonTitle)
                }))
            }else if buttonTitle == "Remove" {
                alertController.addAction(UIAlertAction(title: buttonTitle, style: .destructive, handler: { (action: UIAlertAction) in
                    userAction?(buttonTitle)
                }))
            }else if buttonTitle == "Reject" {
                alertController.addAction(UIAlertAction(title: buttonTitle, style: .destructive, handler: { (action: UIAlertAction) in
                    userAction?(buttonTitle)
                }))
            }else if buttonTitle == "Discard" {
                alertController.addAction(UIAlertAction(title: buttonTitle, style: .destructive, handler: { (action: UIAlertAction) in
                    userAction?(buttonTitle)
                }))
            }else if buttonTitle == "Remove video" {
                alertController.addAction(UIAlertAction(title: buttonTitle, style: .destructive, handler: { (action: UIAlertAction) in
                    userAction?(buttonTitle)
                }))
            }else if buttonTitle == "Remove request" {
                alertController.addAction(UIAlertAction(title: buttonTitle, style: .destructive, handler: { (action: UIAlertAction) in
                    userAction?(buttonTitle)
                }))
            }else if buttonTitle == "Delete" {
                alertController.addAction(UIAlertAction(title: buttonTitle, style: .destructive, handler: { (action: UIAlertAction) in
                    userAction?(buttonTitle)
                }))
            }else if buttonTitle == "Unblock" {
                alertController.addAction(UIAlertAction(title: buttonTitle, style: .destructive, handler: { (action: UIAlertAction) in
                    userAction?(buttonTitle)
                }))
            }else if buttonTitle == "Block" {
                alertController.addAction(UIAlertAction(title: buttonTitle, style: .destructive, handler: { (action: UIAlertAction) in
                    userAction?(buttonTitle)
                }))
            }else if buttonTitle == "Cancel" {
                alertController.addAction(UIAlertAction(title: buttonTitle, style: .cancel, handler: { (action: UIAlertAction) in
                    userAction?(buttonTitle)
                }))
            }else{
                alertController.addAction(UIAlertAction(title: buttonTitle, style: .default, handler: { (action: UIAlertAction) in
                    userAction?(buttonTitle)
                }))
            }
        }
        
        
        if let parentController = controller {
            DispatchQueue.main.async {
                parentController.present(alertController, animated: true, completion: nil)
            }
        }
    }
}

