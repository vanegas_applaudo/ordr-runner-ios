//
//  Utility.swift
//  HealthCarePatient
//
//  Created by XcelTec-036 on 21/05/20.
//  Copyright © 2020 XcelTec-036. All rights reserved.
//

import UIKit

class Utility: NSObject {
    
// AlertView
    class func setAlertWith(title:String?, message:String?, controller: UIViewController, completion: (()->Void)? = nil)  {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (alertAction) in
            // Nothing
        }))
        controller.present(alertController, animated: true, completion: nil)
    }
}

