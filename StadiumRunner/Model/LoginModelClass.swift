//
//  LoginModelClass.swift
//  StadiumUser
//
//  Created by XcelTec-053 on 15/04/21.
//  Copyright © 2021 XcelTec-053. All rights reserved.
//

import UIKit

class LoginModelClass: NSObject {
    let code:Int
    let message:String
    var data:LoginModelData? = nil
    
    init(fromDictioanry dict:NSDictionary)
    {
        code = dict.getIntValue(key: "success")
        message = dict.getStringValue(key:MESSAGE)
        if let getData = dict.value(forKey: DATA) as? String
        {
            let DES = CryptoJS.AES()
            let decryptedString = DES.decrypt(getData, password: "rswna0hu8t")
            print(decryptedString)
            
            let DecryptedData = decryptedString.data(using: .utf8)!
            do{
                let output = try JSONSerialization.jsonObject(with: DecryptedData, options: .allowFragments) as? NSDictionary
                data = LoginModelData.init(fromDictionary: output!)
            }
            catch {
                UIApplication.getTopViewController()?.view.makeToast(error.localizedDescription, duration: TOAST_TIME, position: .bottom,style:ToaststyleRed)
            }
        }
    }
}


class LoginModelData:NSObject {

    var contact_number:String?
    var country_code:String?
    var country_code_name:String?
    var email:String?
    var full_name:String?
    var token:String?
    var city:String?
    var country:String?
    var state:String?
    var user_id:String?
    var user_image:String?
    var country_id:String?
    var state_id:String?
    var city_id:String?
    var user_type:Int?
    init(fromDictionary dict:NSDictionary) {
 
        contact_number = dict.getStringValue(key: "mobile")
        country_code = dict.getStringValue(key: "country_code")
        country_code_name = dict.getStringValue(key: "country_code_name")
        country = dict.getStringValue(key: "country")
        email = dict.getStringValue(key: "email")
        city = dict.getStringValue(key: "city")
        full_name = dict.getStringValue(key: "username")
        state = dict.getStringValue(key: "state")
        token = dict.getStringValue(key: "token")
        user_id = dict.getStringValue(key: "user_id")
        user_image = dict.getStringValue(key: "user_image")
        country_id = dict.getStringValue(key: "country_id")
        state_id = dict.getStringValue(key: "state_id")
        city_id = dict.getStringValue(key: "city_id")
        user_type = dict.getIntValue(key: "user_type")
    }
}
