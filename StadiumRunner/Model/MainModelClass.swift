//
//  MainModelClass.swift
//  StadiumUser
//
//  Created by XcelTec-053 on 16/04/21.
//  Copyright © 2021 XcelTec-053. All rights reserved.
//

import UIKit

class MainModelClass: NSObject {
    var code:NSNumber!
    var message:String!
    var dataDict:NSDictionary!
    var dataArr:NSArray!
    
    init(data:NSDictionary){
        
        if let tempStr = data.value(forKey: MESSAGE)as? String
        {
            message = tempStr
        }
        if let tempNum = data.value(forKey: "success_code")as? NSNumber
        {
            code = tempNum
        }else{
            if let tempNum = data.value(forKey: "success")as? NSNumber
            {
                code = tempNum
            }
        }
       
//        if let dict = data.value(forKey: DATA)as? NSDictionary
//        {
//            dataDict = dict
//        }
//        if let arr = data.value(forKey: DATA)as? NSArray
//        {
//            dataArr = arr
//        }
        
        if let getData = data.value(forKey: DATA) as? String
        {
            let DES = CryptoJS.AES()
            let decryptedString = DES.decrypt(getData, password: "rswna0hu8t")
            print(decryptedString)
            
            let DecryptedData = decryptedString.data(using: .utf8)!
            do{
                let output = try JSONSerialization.jsonObject(with: DecryptedData, options: .allowFragments) as? NSDictionary
                dataDict = output!
            }
            catch {
                UIApplication.getTopViewController()?.view.makeToast(error.localizedDescription, duration: TOAST_TIME, position: .bottom,style:ToaststyleRed)
            }
        }
    }
}
