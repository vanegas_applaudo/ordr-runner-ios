//
//	NotificationData.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 

class NotificationData{

	var descriptionField : String!
	var id : Int!
	var title : String!
	var userId : String!
    var is_read : String!
    var created_date : String!
    
	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
    
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		descriptionField = json["description"].stringValue
		id = json["id"].intValue
		title = json["title"].stringValue
		userId = json["user_id"].stringValue
        
        is_read = json["is_read"].stringValue
        created_date = json["created_date"].stringValue
	}
}
