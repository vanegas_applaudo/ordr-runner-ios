//
//  orderListModelClass.swift
//  StadiumRunner
//
//  Created by XcelTec-53 on 13/08/21.
//  Copyright © 2021 XcelTec-053. All rights reserved.
//

import Foundation


/*

class AcceptOrderListModel : NSObject, NSCoding{

    var createdDate : String!
    var firstName : String!
    var floorId : AnyObject!
    var floorName : AnyObject!
    var lastName : String!
    var orderId : Int!
    var products : [Product]!
    var restaurantName : String!
    var row : AnyObject!
    var rowId : AnyObject!
    var seat : AnyObject!
    var seatId : AnyObject!
    var sectionId : AnyObject!
    var sectionName : AnyObject!


    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        createdDate = json["created_date"].stringValue
        firstName = json["first_name"].stringValue
        floorId = json["floor_id"].stringValue
        floorName = json["floor_name"].stringValue
        lastName = json["last_name"].stringValue
        orderId = json["order_id"].intValue
        products = [Product]()
        let productsArray = json["products"].arrayValue
        for productsJson in productsArray{
            let value = Product(fromJson: productsJson)
            products.append(value)
        }
        restaurantName = json["restaurant_name"].stringValue
        row = json["row"].stringValue
        rowId = json["row_id"].stringValue
        seat = json["seat"].stringValue
        seatId = json["seat_id"].stringValue
        sectionId = json["section_id"].stringValue
        sectionName = json["section_name"].stringValue
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if createdDate != nil{
            dictionary["created_date"] = createdDate
        }
        if firstName != nil{
            dictionary["first_name"] = firstName
        }
        if floorId != nil{
            dictionary["floor_id"] = floorId
        }
        if floorName != nil{
            dictionary["floor_name"] = floorName
        }
        if lastName != nil{
            dictionary["last_name"] = lastName
        }
        if orderId != nil{
            dictionary["order_id"] = orderId
        }
        if products != nil{
            var dictionaryElements = [[String:Any]]()
            for productsElement in products {
                dictionaryElements.append(productsElement.toDictionary())
            }
            dictionary["products"] = dictionaryElements
        }
        if restaurantName != nil{
            dictionary["restaurant_name"] = restaurantName
        }
        if row != nil{
            dictionary["row"] = row
        }
        if rowId != nil{
            dictionary["row_id"] = rowId
        }
        if seat != nil{
            dictionary["seat"] = seat
        }
        if seatId != nil{
            dictionary["seat_id"] = seatId
        }
        if sectionId != nil{
            dictionary["section_id"] = sectionId
        }
        if sectionName != nil{
            dictionary["section_name"] = sectionName
        }
        return dictionary
    }

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
         createdDate = aDecoder.decodeObject(forKey: "created_date") as? String
         firstName = aDecoder.decodeObject(forKey: "first_name") as? String
         floorId = aDecoder.decodeObject(forKey: "floor_id") as? AnyObject
         floorName = aDecoder.decodeObject(forKey: "floor_name") as? AnyObject
         lastName = aDecoder.decodeObject(forKey: "last_name") as? String
         orderId = aDecoder.decodeObject(forKey: "order_id") as? Int
         products = aDecoder.decodeObject(forKey: "products") as? [Product]
         restaurantName = aDecoder.decodeObject(forKey: "restaurant_name") as? String
         row = aDecoder.decodeObject(forKey: "row") as? AnyObject
         rowId = aDecoder.decodeObject(forKey: "row_id") as? AnyObject
         seat = aDecoder.decodeObject(forKey: "seat") as? AnyObject
         seatId = aDecoder.decodeObject(forKey: "seat_id") as? AnyObject
         sectionId = aDecoder.decodeObject(forKey: "section_id") as? AnyObject
         sectionName = aDecoder.decodeObject(forKey: "section_name") as? AnyObject

    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
    {
        if createdDate != nil{
            aCoder.encode(createdDate, forKey: "created_date")
        }
        if firstName != nil{
            aCoder.encode(firstName, forKey: "first_name")
        }
        if floorId != nil{
            aCoder.encode(floorId, forKey: "floor_id")
        }
        if floorName != nil{
            aCoder.encode(floorName, forKey: "floor_name")
        }
        if lastName != nil{
            aCoder.encode(lastName, forKey: "last_name")
        }
        if orderId != nil{
            aCoder.encode(orderId, forKey: "order_id")
        }
        if products != nil{
            aCoder.encode(products, forKey: "products")
        }
        if restaurantName != nil{
            aCoder.encode(restaurantName, forKey: "restaurant_name")
        }
        if row != nil{
            aCoder.encode(row, forKey: "row")
        }
        if rowId != nil{
            aCoder.encode(rowId, forKey: "row_id")
        }
        if seat != nil{
            aCoder.encode(seat, forKey: "seat")
        }
        if seatId != nil{
            aCoder.encode(seatId, forKey: "seat_id")
        }
        if sectionId != nil{
            aCoder.encode(sectionId, forKey: "section_id")
        }
        if sectionName != nil{
            aCoder.encode(sectionName, forKey: "section_name")
        }

    }

}
class Product : NSObject, NSCoding{

    var productId : Int!
    var productName : String!
    var type : String!


    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        productId = json["product_id"].intValue
        productName = json["product_name"].stringValue
        type = json["type"].stringValue
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if productId != nil{
            dictionary["product_id"] = productId
        }
        if productName != nil{
            dictionary["product_name"] = productName
        }
        if type != nil{
            dictionary["type"] = type
        }
        return dictionary
    }

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
         productId = aDecoder.decodeObject(forKey: "product_id") as? Int
         productName = aDecoder.decodeObject(forKey: "product_name") as? String
         type = aDecoder.decodeObject(forKey: "type") as? String

    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
    {
        if productId != nil{
            aCoder.encode(productId, forKey: "product_id")
        }
        if productName != nil{
            aCoder.encode(productName, forKey: "product_name")
        }
        if type != nil{
            aCoder.encode(type, forKey: "type")
        }

    }

}
*/
