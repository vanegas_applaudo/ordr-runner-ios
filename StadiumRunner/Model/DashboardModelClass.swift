//
//  DashboardModelClass.swift
//  StadiumUser
//
//  Created by XcelTec-053 on 23/04/21.
//  Copyright © 2021 XcelTec-053. All rights reserved.
//

import UIKit

class DashboardData: NSObject{

    var data : NewOrderData?
    var message : String?
    var success : Bool?
    var successCode : Int?
}

struct NewOrderData: Decodable{

    var getData : [GetData]?
}

struct GetData: Decodable{

    var created_date : String?
    var first_name : String?
    var floor_id : Int?
    var floor_name : String?
    var last_name : String?
    var order_id : Int?
    var products : [NewProduct]?
    var restaurant_name : String?
    var row : String?
    var row_id : Int?
    var seat : String?
    var seat_id : Int?
    var section_id : Int?
    var section_name : String?
    var order_type : String?
    var total_amount : Double?
}

struct NewProduct: Decodable{

    var product_id : Int?
    var product_name : String?
    var type : String?
    var quantity : Int?
}


class BannerModelData:NSObject {
    var id:String?
    var menu_imgs:String?
    
    init(fromDictionary dict:NSDictionary) {
        id = dict.getStringValue(key: "id")
        menu_imgs = dict.getStringValue(key: "restaurant_image")
    }
}

