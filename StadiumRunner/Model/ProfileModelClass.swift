//
//  ProfileModelClass.swift
//  StadiumUser
//
//  Created by XcelTec-053 on 22/04/21.
//  Copyright © 2021 XcelTec-053. All rights reserved.
//

import UIKit

class ProfileModelClass: NSObject {
    let code:Int
    let message:String
    var data:ProfileModelData? = nil
    
    init(fromDictioanry dict:NSDictionary)
    {
        code = dict.getIntValue(key: "success_code")
        message = dict.getStringValue(key:MESSAGE)
        if let getData = dict.value(forKey: DATA) as? String
        {
            let DES = CryptoJS.AES()
            let decryptedString = DES.decrypt(getData, password: "rswna0hu8t")
            print(decryptedString)
            
            let DecryptedData = decryptedString.data(using: .utf8)!
            do{
                let output = try JSONSerialization.jsonObject(with: DecryptedData, options: .allowFragments) as? NSDictionary
                data = ProfileModelData.init(fromDictionary: output!)
            }
            catch {
                UIApplication.getTopViewController()?.view.makeToast(error.localizedDescription, duration: TOAST_TIME, position: .bottom,style:ToaststyleRed)
            }
        }
    }
}


class ProfileModelData:NSObject {
    
    var contact_number:String?
    var country_code:String?
    var country_code_name:String?
    var email:String?
    var full_name:String?
    var token:String?
    var city:String?
    var country:String?
    var state:String?
    var user_id:String?
    var user_image:String?
    var country_id:String?
    
    init(fromDictionary dict:NSDictionary) {
        
        contact_number = dict.getStringValue(key: "mobile")
        country_code = dict.getStringValue(key: "country_code")
        country_code_name = dict.getStringValue(key: "country_code_name")
        country = dict.getStringValue(key: "country")
        email = dict.getStringValue(key: "email")
        city = dict.getStringValue(key: "city")
        full_name = dict.getStringValue(key: "username")
        state = dict.getStringValue(key: "state")
        token = dict.getStringValue(key: "token")
        user_id = dict.getStringValue(key: "id")
        user_image = dict.getStringValue(key: "user_image")
        country_id = dict.getStringValue(key: "country_id")
    }
}

//{"id":51,"first_name":null,"last_name":null,"username":"nishee","email":"nishee@gmail.com","dob":null,"mobile":"7600519883","user_image":null,"country_code_name":"India","country_code":"+91"}/
