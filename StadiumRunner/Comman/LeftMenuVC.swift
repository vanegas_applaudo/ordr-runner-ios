//
//  LeftMenuVC.swift
//  TruffleUser
//
//  Created by XcelTec on 11/03/21.
//  Copyright © 2021 XcelTec-053. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SVProgressHUD

class LeftMenuVC: UIViewController,UITableViewDataSource,UITableViewDelegate {

    @IBOutlet var viewMain: UIView!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var btnLogout: UIButton!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var onBtnProfilePicOutlet:UIButton!

    var mainViewController: UIViewController!
    var sideMenuArr = [String]()
    
    @IBOutlet weak var imgProfile: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        sideMenuArr =  ["My Profile","Change Password","My Order","Availibility"]
        lblUserName.text = USERDEFAULT.string(forKey: Kfull_name)
        lblEmail.text = USERDEFAULT.string(forKey: Kemail)
        btnLogout.roundCorners(corners: [.topRight, .bottomRight], radius: 10)
        
        viewMain.layer.masksToBounds = true
        viewMain.layer.cornerRadius = 30
        viewMain.layer.maskedCorners = [.layerMaxXMaxYCorner,.layerMaxXMinYCorner]
//        let imgurl = USERDEFAULT.value(forKey: Kimage) as! String
//        if imgurl != "" {
//            imgProfile.sd_setImage(with: URL(string: imgurl), completed: nil)
//        }else{
//        }
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }
    
    
    func APIGetProfileData()
    {
        
        let currentUserId = USERDEFAULT.value(forKey: Kuser_id) as? String
       if currentUserId! != nil {
        let apiURL = "\(GETVENDOR_URL)\(currentUserId!)"
        NetworkManager.shared.getAPICallWithToken(Api:apiURL, getView: self) { [self] (response) in
            if let getdata = response as? NSDictionary {
                print(getdata)
                if JSON(getdata.value(forKey: "success") as Any).boolValue{
                    if let data = getdata.value(forKey: DATA) as? String{
                        let DES = CryptoJS.AES()
                        let decryptedString = DES.decrypt(data, password: "rswna0hu8t")
                        print(decryptedString)
                        
                        let DecryptedData = decryptedString.data(using: .utf8)!
                        do{
                            let output = try JSONSerialization.jsonObject(with: DecryptedData, options: .allowFragments)
                            print(output)

                            let dic = JSON(output as Any).dictionaryValue

//                            print(dic["restaurant_name"]!.stringValue)
//                            self.lblLocation.text = String(describing: dic["restaurant_name"]!.stringValue)
//                            delivery_charge = dic["delivery_charge"]!.floatValue
                            
//                            service_charge = dic["service_charge"]!.floatValue
                            
                          
                            //lblContactNumber.text = phoneNO
                            let profileUrl = dic["profile"]!.stringValue
                            
                            imgProfile.sd_setImage(with: URL(string: profileUrl), completed: nil)
                            USERDEFAULT.set(profileUrl, forKey: Kimage)
                            
                            let first_name = dic["first_name"]!.stringValue
                            let last_name = dic["last_name"]!.stringValue
                            let fullName = "\(first_name) \(last_name)"
                            USERDEFAULT.setValue(fullName, forKey: Kfull_name)

                            lblUserName.text = fullName
                            /*
                             "city_id" = 1;
                             "country_id" = 1;
                             "first_name" = john;
                             id = 1;
                             "is_active" = Y;
                             "last_name" = doe;
                             profile = "";
                             "restaurant_id" = 3;
                             "stadium_id" = 13;
                             "state_id" = 1;
                             status = Busy;
                             username = "john doe";
                             "userrole_id" = 3;
                             "vendor_contact_no" = 6354915520;
                             "vendor_description" = "Fast Delivery";
                             "vendor_email" = "johndoe@mail.com";
                             */
                        }catch{
                            debugPrint(error.localizedDescription)
                        }
                    }else{
                        
                    }
                }else{
                    self.view.makeToast(JSON(getdata.value(forKey: "message") as Any).stringValue, duration: TOAST_TIME, position: .bottom,style:ToaststyleRed)
                }
            } else {
                print("Response is not in json")
            }

        }
    }
       /* NetworkManager.shared.apiCallWithToken(Api: CHANGEPASS_URL, param: parameter, getView: self) { (response) in
            if let getdata = response as? NSDictionary
            {
                print(getdata)
                let model = MainModelClass.init(data: getdata)
                if model.code == 200
                {
                    self.view.makeToast(model.message, duration: 2.0, position: .bottom,style:ToaststyleGreen)

                    self.goLogin()
                }else if model.code == 400 || model.code == 401 || model.code == 403   {
                    self.goToLogin()
                }else{
                    self.view.makeToast(model.message, duration: TOAST_TIME, position: .bottom,style:ToaststyleRed)
                }
            }else{
                self.view.makeToast(msgSomethingWentWrong, duration: TOAST_TIME, position: .bottom,style:ToaststyleRed)
            }
        }*/
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        imgProfile.layer.masksToBounds = true
        imgProfile.layer.cornerRadius = onBtnProfilePicOutlet.frame.size.width/2
       // onBtnProfilePicOutlet.sd_setImage(with: URL.init(string: USERDEFAULT.string(forKey: Kimage)!), for: .normal)
        APIGetProfileData()

    }
    
    //MARK: - Table view delegates
    @IBAction func onBtnLogoutActions(_ sender: UIButton) {
        let alert = UIAlertController.init(title: "Are you sure you want to Logout?", message: "", preferredStyle: .alert)
        
        let yesAction = UIAlertAction.init(title: "Yes", style: .default, handler: {
            (alert) in
            if Helper.sharedHelper.isNetworkAvailable()
            {
                Helper.sharedHelper.goToLogin()
            }
            else{
                APP_DELEGATE.window?.makeToast(msgInternet, duration: TOAST_TIME, position: .bottom)
            }
        })
        
        let noAction = UIAlertAction.init(title: "No", style: .default, handler: { (alert) in
            self.dismiss(animated: true, completion: nil)
        })
        
        alert.addAction(yesAction)
        alert.addAction(noAction)
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sideMenuArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "sideTblCell") as! sideMenuTblCell
        cell.iconImgView.image = UIImage (named: "ic_SideMenu_\(indexPath.row + 1)")
        cell.lblName.text = sideMenuArr[indexPath.row]
        if indexPath.row == 3 {
            
            cell.switchAvailability.isHidden = false
            if let status = USERDEFAULT.value(forKey: Kavailability_status) {
                if status as? String == "true" {
                    cell.switchAvailability.isOn = true
                    cell.switchAvailability.thumbTintColor = .white

                }else{
                    cell.switchAvailability.isOn = false
                    cell.switchAvailability.thumbTintColor = .red

                }
            }
        } else {
            cell.switchAvailability.isHidden = true
        }
        cell.switchAvailability.addTarget(self, action: #selector(availability_Switch_Status(sender:)), for: .valueChanged)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
        
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0
        {
            let vc = Main_STORY.instantiateViewController(withIdentifier: "MyProfileVC") as! MyProfileVC
             self.slideMenuController()?.changeMainViewController(vc, close: true)
        }
        else if indexPath.row == 1
        {
            let vc = Main_STORY.instantiateViewController(withIdentifier: "ChangePasswordVC") as! ChangePasswordVC
            self.slideMenuController()?.changeMainViewController(vc, close: true)
        }
        else if indexPath.row == 2
        {
            let vc = Main_STORY.instantiateViewController(withIdentifier: "MyOrderListVC") as! MyOrderListVC
            self.slideMenuController()?.changeMainViewController(vc, close: true)
        }
    }
    
    @objc func availability_Switch_Status(sender: UISwitch) {
        
        if sender.isOn {
            sender.thumbTintColor = .white
            self.UserAvailability_API(status: "Y")
            self.view.makeToast("Runner Activated Successfully", duration: TOAST_TIME, position: .bottom,style:ToaststyleGreen)
            
        } else {
            sender.thumbTintColor = .red
            
            self.UserAvailability_API(status: "N")
            self.view.makeToast("Runner Deactivated Successfully", duration: TOAST_TIME, position: .bottom,style:ToaststyleRed)
        }
        
    }
    
//    func logout()
//    {
//        let alert = UIAlertController.init(title: "Are you sure you want to Logout?", message: "", preferredStyle: .alert)
//
//        let yesAction = UIAlertAction.init(title: "Yes", style: .default, handler: {
//            (alert) in
//            if Helper.sharedHelper.isNetworkAvailable()
//            {
//                self.logOutUser()
//            }
//            else{
//                APP_DELEGATE.window?.makeToast(msgInternet, duration: TOAST_TIME, position: .bottom)
//            }
//        })
//
//        let noAction = UIAlertAction.init(title: "No", style: .default, handler: { (alert) in
//            self.dismiss(animated: true, completion: nil)
//        })
//
//        alert.addAction(yesAction)
//        alert.addAction(noAction)
//
//        self.present(alert, animated: true, completion: nil)
//    }
}

extension LeftMenuVC {
 
    private func UserAvailability_API(status: String) {
        var parameter = [String: Any]()
        let runnerId = USERDEFAULT.value(forKey: Kuser_id)
        parameter = ["runner_id":runnerId ?? "0",
                     "is_active": status] as [String : Any]
        debugPrint(parameter)
        
        if status == "Y" {
            USERDEFAULT.setValue("true", forKey: Kavailability_status)
        } else if status == "N"{
            USERDEFAULT.setValue("false", forKey: Kavailability_status)
        }
        NetworkManager.shared.apiCallWithToken(Api: RUNNERTOGGLE_URL, param: parameter , getView: self) { (response) in
            if let getdata = response as? NSDictionary {
                print(getdata)
                
                if let data = getdata.value(forKey: DATA) as? String{
                    let DES = CryptoJS.AES()
                    let decryptedString = DES.decrypt(data, password: "rswna0hu8t")
                    print(decryptedString)
                    
                    let DecryptedData = decryptedString.data(using: .utf8)!
                    do{
                        //let ModelData = try JSONDecoder().decode(Availability.self, from: DecryptedData)
                        
                        
                    }catch{
                        debugPrint(error.localizedDescription)
                    }
                } else {
                    print("No Data")
                }
            } else {
                print("Response is not in json")
            }
        }
    }
}

//MARK:- Sidemenu cell
class sideMenuTblCell : UITableViewCell {
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var iconImgView: UIImageView!
    @IBOutlet weak var lineView: UIView!
    @IBOutlet weak var switchAvailability: UISwitch!
    
}

extension UIButton {
    func roundCorners(corners: UIRectCorner, radius: Int = 8) {
        let maskPath1 = UIBezierPath(roundedRect: bounds,
                                     byRoundingCorners: corners,
                                     cornerRadii: CGSize(width: radius, height: radius))
        let maskLayer1 = CAShapeLayer()
        maskLayer1.frame = bounds
        maskLayer1.path = maskPath1.cgPath
        layer.mask = maskLayer1
    }
}

struct Availability : Decodable{
    var message:String?
    var success:String?
    var success_code : Int?
}
/*{
    "message": "Vendor deactive Successfully.",
    "success": true,
    "success_code": 200
}
*/

