//
//  AppDelegate.swift
//  StadiumRunner
//
//  Created by XcelTec-053 on 02/04/21.
//  Copyright © 2021 XcelTec-053. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import SVProgressHUD
import Firebase
import UserNotifications
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    static let shared = UIApplication.shared.delegate as! AppDelegate

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        IQKeyboardManager.shared.enable = true
        
        SVProgressHUD.setDefaultMaskType(.black)
        FirebaseApp.configure()
        
        Messaging.messaging().delegate = self
        

        if USERDEFAULT.value(forKey:Kuser_id) == nil{
            Helper.sharedHelper.goToLogin()
        }else{
            //Helper.sharedHelper.gotoNotification()
            Helper.sharedHelper.goToHome()
        }
        
        settingPushNotification()
        return true
    }
    func settingPushNotification() {
        
        let app = UIApplication.shared
        
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            app.registerUserNotificationSettings(settings)
        }
        
        app.registerForRemoteNotifications()

    }
    func application(_ application: UIApplication,
                     didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
//        debugPrint("PUSHHHH",deviceToken)
        Messaging.messaging().apnsToken = deviceToken
    }
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
//        debugPrint("Firebase registration token: \(String(describing: fcmToken))")
        USERDEFAULT.setValue(fcmToken, forKey: "fcmtoken")
    }
}

extension AppDelegate: UNUserNotificationCenterDelegate, MessagingDelegate{
    
    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        let userInfo = notification.request.content.userInfo
        print(userInfo)
        
        if #available(iOS 14.0, *) {
            completionHandler([[.banner, .badge ,.sound]])
        } else {
            // Fallback on earlier versions
            completionHandler([[.alert, .badge ,.sound]])
        }
    }
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
    
        // Print full message.
        print(userInfo)
        
        completionHandler(UIBackgroundFetchResult.newData)
    }

    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
      let userInfo = response.notification.request.content.userInfo

        if USERDEFAULT.value(forKey:Kuser_id) == nil{
            Helper.sharedHelper.goToLogin()
        }else{
            Helper.sharedHelper.goToNotification()
        }
        
      print(userInfo)

      completionHandler()
    }
}
