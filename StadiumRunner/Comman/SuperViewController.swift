//
//  SuperViewController.swift
//  StadiumRunner
//
//  Created by XcelTec-053 on 02/04/21.
//  Copyright © 2021 XcelTec-053. All rights reserved.
//

import UIKit

class SuperViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false

        // Do any additional setup after loading the view.
    }

    @IBAction func btnMenuOpenActions(_ sender: UIButton) {
        self.slideMenuController()?.openLeft()
    }
    
    @IBAction func btnBackActions(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func goToHome()
    {
        let navigation = Main_STORY.instantiateViewController(withIdentifier: "dashNav") as! UINavigationController
        navigation.isNavigationBarHidden = true
        let mainViewController = Main_STORY.instantiateViewController(withIdentifier: "DashboardVC") as! DashboardVC
        let leftViewController = Main_STORY.instantiateViewController(withIdentifier: "LeftMenuVC") as! LeftMenuVC
        var slideMenuController = SlideMenuController()
        slideMenuController = SlideMenuController(mainViewController: navigation, leftMenuViewController: leftViewController)
        slideMenuController.delegate = mainViewController as? SlideMenuControllerDelegate
        self.navigationController?.pushViewController(slideMenuController, animated: false)
    }

    
    func goToLogin()
    {
        USERDEFAULT.removeObject(forKey: Kuser_id)
        let vc = storyboard?.instantiateViewController(withIdentifier: "LoginVC")as! LoginVC
        navigationController?.pushViewController(vc, animated: true)
    }

    
}
